---
widget: blank  # See https://sourcethemes.com/academic/docs/page-builder/
headless: true  # This file represents a page section.
active: true  # Activate this widget? true/false
weight: 10  # Order that this section will appear.

title: "Conference series"
subtitle: "International Conference Series on CMC and Social Media Corpora"

design:
  columns: '2'
---

The International Conference Series on CMC and Social Media Corpora
([*cmc-corpora*](/)) is a series of conferences
[established in 2013](/conference/cmc-corpora2013/) and dedicated to the
collection, annotation, processing, and analysis of corpora of
computer-mediated communication (CMC) and social media.

The conferences bring together language-centered research on CMC and social
media in linguistics, philologies, communication sciences, media, and social
sciences with research questions from the fields of corpus and computational
linguistics, language technology, text technology, and machine learning. The
conferences feature research in which computational methods and tools are used
for language-centered empirical analysis of CMC and social media phenomena as
well as research on building, processing, annotating, representing, and
exploiting CMC and social media corpora, including their integration in digital
research infrastructures.
