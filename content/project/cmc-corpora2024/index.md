---
#draft: true
slides: ""
url_pdf: ""
summary: ""
section_pager: true
date_end: 2024-09-06
author: []
url_video: ""
title: "CMC2024: 11th Conference on Computer-Mediated Communication (CMC) and
  Social Media Corpora"
subtitle: "5-6 September 2024 in Nice (France)"
date: 2023-12-18
featured: true
tags: []
url_slides: ""
address:
  city: Nice
  country: France
links:
  - icon: globe-europe
    icon_pack: fas
    url: https://cmc-corpora-nice.sciencesconf.org/
  - icon: twitter
    icon_pack: fab
    url: https://twitter.com/cmc_corpora
event: cmc2024
publishDate: 2023-12-18T00:00:00Z
url_code: ""
all_day: true
share: false
image:
  preview_only: true
---
![Group Photo](featured.jpg "Group Photo")

The 11th Conference on Computer-Mediated Communication (CMC) and Social Media
Corpora was held on Thursday, the 5th and Friday, the 6th of September 2024 at
the Université Côte d'Azur (Nice, France). 

The conference brought together language-centered research on CMC and social
media in linguistics, philologies, communication sciences, media, foreign
language teaching and learning, and social sciences with research questions
from the fields of corpus and computational linguistics, language technology,
text technology, and machine learning. It featured research in which
computational methods and tools are used for language-centered empirical
analysis of CMC and social media phenomena as well as research on building,
processing, annotating, representing, and exploiting CMC and social media
corpora, including their integration in digital research infrastructures.
