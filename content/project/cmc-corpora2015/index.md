---
title: "CMC-Corpora 2015: 3rd Conference on CMC and Social Media Corpora for the Humanities"
subtitle: "23-24 Oct 2015 @ University of Rennes (France)"
summary: ""
tags: []
date: "2015-10-25"

# Optional external URL for project (replaces project detail page).
external_link: ""

image:
  caption:
  preview_only: false

links:
- icon: globe-europe
  icon_pack: fas
  url: http://ird-cmc-rennes.sciencesconf.org/
url_code: ""
url_pdf: ""
url_slides: ""
url_video: ""

share: false

section_pager: true

# Slides (optional).
#   Associate this project with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides = "example-slides"` references `content/slides/example-slides.md`.
#   Otherwise, set `slides = ""`.
slides: ""

author: []
---

The international research days (IRDs) on Social Media and CMC Corpora for the
eHumanities were held in Rennes, France on 23-24th October 2015 and focused on
communication and interactions stemming from networks such as the Internet or
telecommunications, as well as mono and multimodal, synchronous and
asynchronous communications. The focus of the IRD encompassed different CMC
genres. These included, but were not limited to, discussion forums, email, SMS,
text chat, wiki discussions, discussions in multimodal and/or 3D environments.

The aim of the IRDs was to bring together researchers who had collected CMC
data and who wished to organize and share them for research purposes. The IRDs
focused on the process of building CMC corpora including annotation and
analysis processes as well as the  questions of ethics and rights raised by
publishing CMC corpora as open data.  
