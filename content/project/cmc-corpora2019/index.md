---
title: "CMC-Corpora 2019: 7th Conference on Computer-Mediated Communication (CMC) and Social Media Corpora"
subtitle: "9-10 Sep 2019 @ Paris Seine University, Cergy-Pontoise (France)"
summary: ""
tags: []
date: "2019-09-09"

# Optional external URL for project (replaces project detail page).
# external_link: "https://cmccorpora19.sciencesconf.org/"
external_link: ""

links:
- icon: globe-europe
  icon_pack: fas
  url: https://cmccorpora19.sciencesconf.org/
- icon: twitter
  icon_pack: fab
  url: https://twitter.com/hashtag/cmccorpora19
url_code: ""
url_pdf: ""
url_slides: ""
url_video: ""

share: false

section_pager: true

# Slides (optional).
#   Associate this project with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides = "example-slides"` references `content/slides/example-slides.md`.
#   Otherwise, set `slides = ""`.
slides: ""

author: []
image:
  preview_only: true
---
![Group Photo](featured.jpg "Group Photo")

'CMC-Corpora 2019' was the 7th edition of an annual conference series dedicated
to the collection, annotation, processing and exploitation of corpora of
computer-mediated communication (CMC) and social media for research in the
humanities. The conference brought together language-centered research on CMC
and social media in linguistics, philologies, communication sciences, media and
social sciences with research questions from the fields of corpus and
computational linguistics, language technology, text technology, and machine
learning.
