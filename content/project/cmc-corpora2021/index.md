---
title: "CMC2021: 8th Conference on Computer-Mediated Communication (CMC) and Social Media Corpora"
subtitle: "28-29 Oct 2021 @ Radboud University, Nijmegen (Netherlands) and Online"
summary: ""
tags: []
date: "2021-10-28"

# Schedule page publish date (NOT talk date).
#publishDate: "2017-01-01T00:00:00Z"
#featured: true

external_link: ""

links:
- icon: globe-europe
  icon_pack: fas
  url: https://applejack.science.ru.nl/cmc2021/ 
- icon: twitter
  icon_pack: fab
  url: https://twitter.com/search?q=cmc-corpora
url_code: ""
url_pdf: ""
url_slides: ""
url_video: ""
all_day: true
share: false

section_pager: true

# Slides (optional).
#   Associate this project with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides = "example-slides"` references `content/slides/example-slides.md`.
#   Otherwise, set `slides = ""`.
slides: ""

author: []
image:
  preview_only: true
---
![Group Photo](featured.jpg "Group Photo")

The 8th edition of the Conference on Computer-Mediated Communication (CMC) and
Social Media Corpora took place in Nijmegen, the Netherlands on Thursday
28 - Friday 29 October 2021.

CMC2021 was the 8th edition of an annual conference series dedicated to the
development, analysis, and processing of corpora of computer-mediated
communication (CMC) and social media for research in the humanities. The
conference brought together language-centred research on CMC and social media in
linguistics, communication sciences, media studies, and social sciences with
research questions from the fields of corpus and computational linguistics,
language and text technology, and machine learning. Because of the global
COVID-19 pandemic, the conference took place in hybrid form.
