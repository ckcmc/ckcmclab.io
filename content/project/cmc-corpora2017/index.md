---
title: "CMC-Corpora 2017: 5th Conference on CMC and Social Media Corpora for the Humanities"
subtitle: "3-4 Oct 2017 @ Eurac Research, Bolzano (Italy)"
summary: ""
tags: []
date: "2017-10-03"

# Optional external URL for project (replaces project detail page).
# external_link: "https://cmc-corpora2017.eurac.edu/"
external_link: ""

links:
- icon: globe-europe
  icon_pack: fas
  url: https://cmc-corpora2017.eurac.edu/
- icon: twitter
  icon_pack: fab
  url: https://twitter.com/hashtag/cmccorpora17
url_code: ""
url_pdf: ""
url_slides: ""
url_video: ""

share: false

section_pager: true

# Slides (optional).
#   Associate this project with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides = "example-slides"` references `content/slides/example-slides.md`.
#   Otherwise, set `slides = ""`.
slides: ""

author: []
image:
  preview_only: true
---
![Group Photo](featured.jpg "Group Photo")

The 5th conference on CMC and Social Media Corpora for the Humanities was
held in Bolzano/Bozen, Italy, 3-4 October 2017 and focused on the
collection, analysis and processing of mono and multimodal, synchronous and
asynchronous communications. The focus encompassed different CMC genres.
These included, but were not limited to, discussion forums, blogs, newsgroups,
emails, SMS and WhatsApp, text chats, wiki discussions, social network
exchanges (such as Facebook, Twitter, Linkedin), discussions in multimodal
and/or 3D environments (virtual worlds, gaming worlds).

The conference brought together researchers with interests in the
collection, organization, processing, analysis and sharing of CMC data for
research purposes. 

The conference was hosted by Eurac Research and included a post-conference
workshop on using the TEI for annotating CMC and social media resources (4
October 2017). It was followed by the 4th Learner Corpus Research Conference and
its preceding conference workshop, which were held at the same venue
from 4-7 October 2017.
