---
#draft: true
slides: ""
url_pdf: ""
summary: ""
section_pager: true
date_end: 2022-09-29
authors:
- Steven Coats
- Egon W. Stemle
profile: false
url_video: ""
title: "CMC2022: 9th Conference on Computer-Mediated Communication (CMC) and Social Media Corpora"
subtitle: "28-29 Sep 2022 in Santiago de Compostela (Spain)"
date: 2022-09-28
featured: true
tags: []
url_slides: ""
address:
  city: Santiago de Compostela
  country: Spain
links:
  - icon: globe-europe
    icon_pack: fas
    url: https://www.usc.es/en/congresos/cmc2022
  - icon: twitter
    icon_pack: fab
    url: https://twitter.com/cmc_corpora
event: cmc2022
publishDate: 2017-01-01T00:00:00Z
url_code: ""
all_day: true
share: false
image:
  preview_only: true
---
![Group Photo](featured.jpg "Group Photo")

The 9th Conference on Computer-Mediated Communication (CMC) and Social Media Corpora was held in Santiago de Compostela, Spain, 28-29 September 2022. It was be hosted by the Faculty of Philology of the University of Santiago de Compostela.

‘CMC-corpora 2022’ was the 9th edition of an annual conference series dedicated to the development, analysis, and processing of corpora of computer-mediated communication and social media for research in the humanities. The conference brought together language-centred research on CMC and social media in linguistics, communication sciences, media studies, and social sciences with research questions from the fields of corpus and computational linguistics, language and text technology, and machine learning.
