---
title: "CMC-Corpora 2018: 6th conference on CMC and Social Media Corpora"
subtitle: "17-18 Sep 2018 @ University of Antwerp (Belgium)"
summary: ""
tags: []
date: "2018-09-19"

# Optional external URL for project (replaces project detail page).
#
external_link: ""

links:
#- icon: globe-europe
#  icon_pack: fas
#  url: https://www.uantwerpen.be/en/conferences/cmc-social-media-2018/call-for-papers/
- icon: twitter
  icon_pack: fab
  url: https://twitter.com/hashtag/cmccorpora18
url_code: ""
url_pdf: ""
url_slides: ""
url_video: ""

share: false

section_pager: true

# Slides (optional).
#   Associate this project with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides = "example-slides"` references `content/slides/example-slides.md`.
#   Otherwise, set `slides = ""`.
slides: ""

author: []
image:
  preview_only: true
---
![Group Photo](featured.jpg "Group Photo")

'CMC-corpora 2018' was the 6th edition of an annual conference series dedicated
to the collection, annotation, processing and exploitation of corpora of
computer-mediated communication (CMC) and social media for research in the
humanities. The conference brought together language-centered research on CMC
and social media in linguistics, philologies, communication sciences, media and
social sciences with research questions from the fields of corpus and
computational linguistics, language technology, text technology, and machine
learning.

The 6th conference on CMC and Social Media Corpora was held in Antwerp,
Belgium on 17-18 September 2018. It was hosted by the [CLiPS research center
(Computational Linguistics and
Psycholinguistics)](https://www.uantwerpen.be/en/research-groups/clips/) at the
University of Antwerp.
