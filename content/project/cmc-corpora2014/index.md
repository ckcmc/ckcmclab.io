---
title: "2014: Social Media Corpora for the eHumanities: Standards, Challenges, and Perspectives"
subtitle: "20-21 Feb 2014 @ TU Dortmund University (Germany)"
summary: ""
tags: []
date: "2014-02-22"

# Optional external URL for project (replaces project detail page).
external_link: ""

image:
  caption:
  preview_only: false

url_code: ""
url_pdf: ""
url_slides: ""
url_video: ""

share: false

section_pager: true

# Slides (optional).
#   Associate this project with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides = "example-slides"` references `content/slides/example-slides.md`.
#   Otherwise, set `slides = ""`.
slides: ""

author: []
---

Final conference of the DFG Scientific Network Empirikom & 2nd Conference on CMC and Social Media Corpora for the Humanities.
