---
title: "CMC2025: 12th Conference on Computer-Mediated Communication (CMC) and
  Social Media Corpora"
subtitle: 4-5 September 2025 in Bayreuth, Germany
date: 2024-12-17T16:59:56.225Z
draft: false
featured: false
external_link: https://www.cmc2025.uni-bayreuth.de/en/
image:
  filename: featured
  focal_point: Smart
  preview_only: false
---
<!--StartFragment-->

The 12th International Conference on CMC and Social Media Corpora for the Humanities (CMC-Corpora) will be held at the **University of Bayreuth**, Germany, on the **4th and 5th of September, 2025**, hosted by the Department of German (Applied) Linguistics. 

T﻿he submission deadline for short papers and abstracts is **15 May 2025**. 

F﻿or more information, please visit <https://www.cmc2025.uni-bayreuth.de/en/>.