---
title: "CMC-Corpora 2016: 4th Conference on CMC and Social Media Corpora for the Humanities"
subtitle: "27-28 Sep 2016 @ Faculty of Arts, University of Ljubljana (Slovenia)"
summary: ""
tags: []
date: "2016-09-29"

# Optional external URL for project (replaces project detail page).
# external_link: "https://cmc-corpora2017.eurac.edu/"
external_link: ""

links:
- icon: globe-europe
  icon_pack: fas
  url: http://nl.ijs.si/janes/cmc-corpora2016/
url_code: ""
url_pdf: ""
url_slides: ""
url_video: ""

share: false

section_pager: true

# Slides (optional).
#   Associate this project with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides = "example-slides"` references `content/slides/example-slides.md`.
#   Otherwise, set `slides = ""`.
slides: ""

author: []
image:
  preview_only: true
---
![Group Photo](featured.jpg "Group Photo")

CMC and Social Media Corpora for the Humanities was the 4th edition of an annual
conference series dedicated to the collection, annotation, processing and
exploitation of corpora of computer-mediated communication (CMC) and social
media for research in the humanities. The conference brought together
language-centered research on CMC and social media in linguistics, philologies,
communication sciences, media and social sciences with research questions from
the fields of corpus and computational linguistics, language technology, text
technology, and machine learning. The conference featured:

* research in which computational methods and tools are used for the empirical
  analysis of CMC in the humanities;
* approaches towards automatic processing and annotation of CMC data with
  computational methods; and
* corpus-linguistic research on collecting, processing, representing and
  providing CMC corpora on the basis of standards in the field of the digital
  humanities.
