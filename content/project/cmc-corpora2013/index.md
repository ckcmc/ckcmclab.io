---
title: "2013: Building Corpora of Computer-Mediated Communication: Issues, Challenges, and Perspectives"
subtitle: "13-15 Feb 2013 @ TU Dortmund University (Germany)"
summary: ""
tags: []
date: "2013-02-15"

# Optional external URL for project (replaces project detail page).
# external_link: "https://cmc-corpora2017.eurac.edu/"
external_link: ""

image:
  caption:
  preview_only: false

links:
- icon: globe-europe
  icon_pack: fas
  url: https://sites.google.com/view/empirikom/aktivit%C3%A4ten/cmccorpora-2013
url_code: ""
url_pdf: ""
url_slides: ""
url_video: ""

share: false

section_pager: true

# Slides (optional).
#   Associate this project with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides = "example-slides"` references `content/slides/example-slides.md`.
#   Otherwise, set `slides = ""`.
slides: ""

author: []
---

The aim of this workshop was to discuss issues raised in the [DFG-Netzwerk
Empirikom](https://sites.google.com/view/empirikom/startseite) concerning the
processing of linguistically annotated CMC corpora for German through an
exchange with corpus projects on other languages.
The workshop presented an overview of work  and desiderata in projects on
German, English, French, Italian, Dutch and Spanish. 
Based on the work presented, points of common interest were explored for which
a transfer of concepts and an elaboration of solutions across languages should
be sought.

Most importantly, this workshop was the initial spark for establishing the
[*International Conference Series on CMC and Social Media Corpora*](/series/),
which has been held annually since 2013.

