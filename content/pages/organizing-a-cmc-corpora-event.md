---
title: 'Organizing a cmc-corpora event'
subtitle: ''
summary: Instructions on how to organize a cmc-corpora event.
authors: []
tags: []
categories: []
date: "2015-01-01T00:00:00"
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Placement options: 1 = Full column width, 2 = Out-set, 3 = Screen-width
# Focal point options: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight
image:
  placement: 2
  caption: ""
  focal_point: ""
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
---

If you'd like to organize a conference in the [cmc-corpora series]({{< relref
"/series" >}}), please get in touch with the [steering committee]({{< relref
"/series#committee" >}}). Send a short **exposé** which covers the scientific
part of the event, as well as details of the local organization – see hereafter
– and a provisional budget.

{{< toc >}}

## Overview
Each conference is organized by **local organizers** in cooperation with the
steering committee. cmc-corpora conferences are announced via the cmc-corpora
mailing list, the cmc-corpora Facebook page and other mailing lists & networks
with a **call for papers** at least six (better: eight) months in advance. Papers
are submitted for the conference via an online submission system (e.g.,
[EasyChair](https://easychair.org/), [ConfTool](https://www.conftool.net/))
which is administered by the local organizers.

The scientific committee has a **chair** which is composed equally of
representatives of the steering committee and representatives of the local
organizers (1:1 or 2:2).

The other members of the scientific committee are chosen cooperatively by the
scientific chairs. It should comprise 10 people at minimum and – for
continuity's sake – include people from the scientific comitee of previous
conferences of the series.

The **conference language is English**. All **submissions are peer-reviewed**
independently by two members of the **scientific committee**. Reviews are
assigned to reviewers by the scientific chairs.

The scientific chairs make sure that submissions accepted for the conference as
result of the review process represent projects from different countries and on
CMC and social media discourse in different languages.

Each conference has a volume of **proceedings** which is made available online
together with the conference and which includes all submissions which have been
accepted for the conference. The proceedings are openly accessible. The local
organizers make sure that the proceedings remain accessible persistently.

It is expected that the organizing institution is able to finance travel and
accomodation for one international **keynote speaker**. A cmccorpora conference
typically has two keynote talks given by one local keynote speaker (= from the
country where the conference takes place) and one international speaker.

Besides the scientific comitee there is an **organization committee** with
members nominated by the local organizers.

## Roadmap for organizing the conference

1. The local organizers report to the steering committee early in advance about
   their plans for local organization (venue, program structure, accommodation)
   in the format of a concise exposé (An example for an exposé of a previous
   conference is attached). Open issues will be discussed with the steering
   committee via skype or email. The exposé should include suggestions for a
   conference date (or dates) which fit for the local organizers and their
   institution.

1. After (1) is finished, the steering committee and the local organizers
   cooperatively nominate the scientific chairs for the conference and define
   dates and formats for submission, reviewing, notifications, and for the
   publication of the online proceedings.

1. The scientific chairs create a call for papers for the conference.

1. The local organizers set up a conference website with the call for papers,
   important dates, the list of committee members and at least preliminary local
   information (venue, travel, etc.) which may later be expanded.

The steering committee is involved as a partner in general aspects of planning
the conference while the main responsibility for the overall organization of
the event is with the local organizers and the responsibility for the
scientific organization is with the scientific chairs.

## Further aspects which the local organizers may wish to address in the exposé

* The program & schedule of the conference is made available on the conference
  website as early in advance as possible.

* Up-to-date technical equipment in the conference venue (beamer, computer for
  presentation, enough room for the expected number of participants).

* Each session for the conference should have a session chair (e.g., members of
  the scientific and of the organization committee).

* If different sessions in the conference are scheduled for different rooms, it
  should be always clear for the participants during the conference where to
  find the room of their choice, and participants should have time to switch
  from one room to the other.

* In the conference breaks, a coffee bar with at least coffee, tea and
  non-alcoholic drinks should be provided. If there's no option for getting the
  coffee bar funded by a third party, it is acceptable to include costs for
  coffee breaks in the general conference fee, or to ask the participants to
  pay a lump sum for coffee bar & refreshments which covers the expenses of the
  organizers.

* For lunch breaks, the local organizers make suggestions where participants
  could go for lunch (or organize to have lunch with the whole group).
  Participants pay the lunch by themselves except that the local organizers
  should have funding to invite them.

* The conference should include a social event (e.g. a conference dinner) which
  is organized by the local organizers. We recommend to let participants decide
  if they wish to attend the social event together with the (online)
  registration. Costs for the social event should not be included in the
  general conference fee but be only paid by those participants who decided to
  attend it.

* On the conference website, the local organizers provide travel directions for
  the participants considering different means of transportation.

* The publication of a monograph with follow-up proceedings is encouraged (but
  not compulsory) to preserve the major outcomes of the conference for the
  scientific community. Whereas the online proceedings which are published
  together with the conference include the short papers submitted for the
  conference, the follow-up allows authors to present their contributions in
  the form of full papers. If a follow-up book is planned we strongly recommend
  to publish it as open access. For the follow-up book, there should be a
  separate call for papers which is published after the conference. The
  scientific chair of the conference decides about the call for papers. The
  call may also address people who have not presented a paper at the
  conference.

## Get in touch 
[contact @ cmc-corpora.org](mailto:contact@cmc-corpora.org)
