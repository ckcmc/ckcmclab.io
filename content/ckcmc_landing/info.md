+++
# Blank widget.
widget = "blank"  # See https://sourcethemes.com/academic/docs/page-builder/
headless = true  # This file represents a page section.
active = true  # Activate this widget? true/false
weight = 10  # Order that this section will appear.

title = "Welcome"
subtitle = '<a href="http://hdl.handle.net/11372/DOC-162"><img src="CLARIN-Logo-K-centre-2023.jpg" alt="CLARIN K-centre Logo" style="height:4em;margin-left:0px;" /></a><ul class="cta-group"> <li> <a href="#contact" class="btn btn-primary text-light px-5 py-2">Contact us</a> </li> </ul>'


[design]
  # Choose how many columns the section has. Valid values: 1 or 2.
  columns = "2"

[design.background]
  # Apply a background color, gradient, or image.
  #   Uncomment (by removing `#`) an option to apply it.
  #   Choose a light or dark text color by setting `text_color_light`.
  #   Any HTML color name or Hex value is valid.

  # Background color.
  # color = "navy"
  
  # Background gradient.
  # gradient_start = "#4bb4e3"
  # gradient_end = "#2b94c3"
  
  # Background image.
  # image = "clarin-k.jpg"  # Name of image in `static/media/`.
  # image_darken = 0.99  # Darken the image? Range 0-1 where 0 is transparent and 1 is opaque.
  image_size = "cover"  #  Options are `cover` (default), `contain`, or `actual` size.
  image_position = "left"  # Options include `left`, `center` (default), or `right`.
  image_parallax = false # Use a fun parallax-like fixed background effect? true/false
  
  # Text color (true=light or false=dark).
  text_color_light = false

[advanced]
  css_class="ckcmc"
+++

## CLARIN K(nowledge)-centre for Computer-Mediated Communication and Social Media Corpora

The [CLARIN Knowledge Centre](https://www.clarin.eu/content/knowledge-centres)
for Computer-Mediated Communication and Social Media Corpora (CKCMC) offers
expertise on language resources and technologies for Computer-Mediated
Communication and Social Media. Its basic activities are to
1. Give researchers, students, and other interested parties information about
   the available resources, technologies, and community activities,
2. Support interested parties in producing, modifying or publishing relevant
   resources and technologies and
3. Organize training activities.


## Computer-Mediated Communication (CMC)

User-generated CMC and social media content offers a wide range of research
opportunities for a growing multidisciplinary research community to examine
themes that often relate to—but are not limited to—the interaction between
language, CMC, and society like, for example, language variation, pragmatics,
media and communication studies. The data is also very important for the
development of robust NLP tools that can deal with non-standard spelling,
vocabulary and grammar. 
Compilation and dissemination of such corpora are hindered by the unclear legal
status of CMC data when distributed as resource to the scientific community,
which is further exacerbated by the rapidly changing terms of service by
content providers.
