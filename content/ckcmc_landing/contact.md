+++
# Blank widget.
widget = "blank"  # See https://sourcethemes.com/academic/docs/page-builder/
headless = true  # This file represents a page section.
active = true  # Activate this widget? true/false
weight = 50  # Order that this section will appear.

title = "Contact us"
subtitle = ""

[design]
  # Choose how many columns the section has. Valid values: 1 or 2.
  columns = "2"

[design.background]
  # Apply a background color, gradient, or image.
  #   Uncomment (by removing `#`) an option to apply it.
  #   Choose a light or dark text color by setting `text_color_light`.
  #   Any HTML color name or Hex value is valid.

  # Background color.
  # color = "#0081ae"
  # color = "#f7feff"
  
  # Background gradient.
  #gradient_start = "#4b4443"
  #gradient_end = "#8bb4e3"
  
+++

Our helpdesk can be contacted via [email to `helpdesk @ THIS
DOMAIN`](mailto:{{< param helpdesk_email >}}). The helpdesk offers additional
clarifications regarding the documentation and support in using, modifying,
producing, or publishing CMC resources and technologies.

<ul class="cta-group">
  <li>
    <a href="mailto:{{< param helpdesk_email >}}" class="btn btn-primary px-3 py-3">Get in touch!</a>
  </li>
  <li>
    <a href="https://gitlab.com/ckcmc/service_desk/-/issues/?sort=created_date&state=all" >
      Check out previous inquiries...<i class="fas fa-angle-right"></i>
    </a>
  </li>
</ul>
