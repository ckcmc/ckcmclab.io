---
# An instance of the Blank widget.
# Documentation: https://sourcethemes.com/academic/docs/page-builder/
widget: blank

# Activate this widget? true/false
active: true

# This file represents a page section.
headless: true

# Order that this section appears on the page.
weight: 99

title: Documentation
subtitle:

design:
  columns: "2"
---

For more detailed information, see the 
[dedicated documentation section](docs).
