---
# An instance of the Blank widget.
# Documentation: https://sourcethemes.com/academic/docs/page-builder/
widget: blank

# Activate this widget? true/false
active: true

# This file represents a page section.
headless: true

# Order that this section appears on the page.
weight: 1

title: 
subtitle:

design:
  columns: "custom"
  background:
    image: headers/DSC_0727.NEF.jpg
    image_darken: 0.8
    image_parallax: false
    image_position: center
    image_size: cover
    text_color_light: true
---

<div class="col-12 col-lg-4 section-heading">
            <h1>Conference Series </h1>
</div>
<div class="col-12 col-lg-8 section-heading">
Series of conferences dedicated to the collection, annotation, processing, and
analysis of corpora of computer-mediated communication (CMC) and social media
corpora.

[More about the Conference Series]({{< relref "/series" >}})
</div>


<div class="col-12 col-lg-4 section-heading">
            <h1>CKCMC</h1>
</div>
<div class="col-12 col-lg-8 section-heading">
The CLARIN K(nowledge)-Centre for Computer-Mediated Communication and Social
Media Corpora (CKCMC) is dedicated to questions about representation,
standardisation and distribution of CMC Corpora.

[More about the CKCMC]({{< relref "/ckcmc_landing" >}})
</div>
