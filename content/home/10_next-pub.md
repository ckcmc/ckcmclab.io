---
# An instance of the Blank widget.
# Documentation: https://sourcethemes.com/academic/docs/page-builder/
widget: featured

# Activate this widget? true/false
active: false

# This file represents a page section.
headless: true

# Order that this section appears on the page.
weight: 10

title: Latest publication
subtitle:

design:
  columns: "2"
  # background:
  #   # image: headers/bubbles-wide.jpg
  #   image_darken: 0.6
  #   image_parallax: true
  #   image_position: center
  #   image_size: cover
  #   # text_color_light: true
  view: 2

content:
  page_type: publication
  count: 2
  order: desc
  filter:
    tag: ''
    category: ''
---
