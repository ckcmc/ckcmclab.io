---
title: Eurac Research, Bolzano/Bozen, IT
subtitle: Institute for Applied Linguistics

share: false
section_pager: true

authors:
- Egon W. Stemle
- Jennifer-Carmen Frey
- Aivars Glaznieks
- Lionel Nicolas

type: ckcmc_partner
---

[Eurac Research](http://www.eurac.edu) addresses the challenges of the future
and seeks answers in the interaction between many different disciplines on
three major themes: regions fit for living in, diversity as a life-enhancing
feature, a healthy society.
<!--more-->
The research of the [Institute for Applied
Linguistics](http://www.eurac.edu/linguistics) aims to provide scientific
answers to issues of language and education policy as well as economic and
social questions at local and international level.  Located in the northern
Italian region of South Tyrol, where practised multilingualism encompasses
geographic, institutional, social and personal aspects, its activities are
focused on applied research (carrying out research projects and networking),
training and consulting (consulting services, monitoring, seminars) as well as
dissemination (scientific publications, dictionaries, data bases, corpora).

## Academic Status and History

Eurac Research was founded in 1992 as an association under private law with
just twelve members of staff undertaking research in the areas of Language and
Law, Minorities and Autonomous Regions as well as the Alpine Environment. The
center gradually expanded its activities into new areas, attracted
scientists from all over the world and introduced new structures. Today, almost
400 scientists from over 25 countries work here.
