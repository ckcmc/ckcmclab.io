---
title: Laboratoire de Linguistique Formelle (LLF), FR
subtitle: Formal Linguistics Laboratory
summary: The Laboratoire de Linguistique Formelle is a French research unit co-sponsored by the [CNRS](http://cnrs.fr) and the [Université of Paris](http://u-paris.fr).

share: false
section_pager: true

# Optional external URL for project (replaces project detail page).
# external_link: https://www.cnrs.fr

image:
  # caption: ""
  # preview_only: true
  focal_point: Smart

authors:
- Achille Falaise

type: ckcmc_partner
---

[The Laboratoire de Linguistique Formelle](http://www.llf.cnrs.fr) studies all aspects of language, from the word to discourse and dialog, from the acoustic signal to interpretation. Its members develop a formal approach to the particular cognitive system that is language, and conduct their research using methods and goals from theoretical linguistics, experimental linguistics, computational linguistics, field linguistics and typology. Animated by an open and collaborative spirit, the Laboratoire de Linguistique Formelle is a member of the Labex EFL - Empirical Foundations of Linguistics, and participates in numerous national and international collaborations. 

LLF's specialty is the combination of three complementary kinds of diversity: diversity of theoretical approaches, diversity of empirical methods, and diversity of languages studied. The unity of the research conducted at LLF lies in the following set of shared beliefs:
* Formal linguistic analysis, which gives the lab its name, is enlightening when it is built on a rich and broad empirical base.
* Linguistic phenomena are most often best understood at the interfaces of traditional scientific disciplines.
* The analysis of the cognitive system of language requires a deep understanding of linguistic diversity

## Academic Status and History

The Laboratoire de Linguistique Formelle is a "mixed research unit" (UMR 7110) co-sponsored by the [CNRS](http://cnrs.fr) and the [Université of Paris](http://u-paris.fr). It is housed by the Université of Paris. Led by Olivier Bonami since March 2016, it is made up of 12 CNRS researchers, 5.5 research and administrative engineers, 22 teaching researchers and 34 PhD students.

Founded in 1972 by Antoine Culioli - originally as an associated research team, then as an associated research unit -, LLF has been led by Jean-Claude Milner (1990-1999), Jean Lowenstamm (1999-2005), Alain Kihm (2005-2011) and Anne Abeillé (2011-2016).
