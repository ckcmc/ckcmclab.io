---
title: Leibniz-Institut für Deutsche Sprache (IDS), Mannheim, DE
subtitle: "Department of Digital Linguistics"
summary: IDS is the central non-university institution for the study and documentation of the contemporary usage and recent history of the German language.

share: false
section_pager: true

# Optional external URL for project (replaces project detail page).
# external_link: https://www.ids-mannheim.de/

image:
  # caption: ""
  # preview_only: true
  focal_point: Smart

authors:
- Harald Lüngen
- Andreas Witt

type: ckcmc_partner
---

[The Leibniz-Institut for the German Language (IDS)](https://www1.ids-mannheim.de) is the central non-university institution for the study and documentation of the contemporary usage and recent history of the German language. Together with more than 90 research and service institutions, it is a member of the Leibniz Association, one of the four major research organizations in Germany. 

In its four research departments (Grammar, Lexis, Pragmatics, and Digital Linguistics), the IDS pursues mostly long-term projects which require larger research teams. Within a number of research projects, the IDS cooperates with project teams and individual researchers at universitites.

Its library, archives, documentations, digital corpora and language repositories are available to external researchers as well. With its lecture events, conferences and colloquia, the IDS is also a place of academic encounter and communication for researchers in German studies from Germany and abroad and all those interested in language.

IDS is a member of [CLARIN-D](https://www.clarin-d.de/en/), the German branch of CLARIN.

## Academic Status and History

The IDS was founded in 1964. As a member of the Leibniz Association, the Institute is financed jointly by the Federal Government and the state of Baden-Württemberg. Various additional funding is provided by research-promotion organizations such as the Deutsche Forschungsgemeinschaft (German Community for the Promotion of Research) and the Volkswagen-Stiftung (Volkswagen Foundation). At present, the IDS has 227 employees including 105 researchers.

