---
title: Jožef Stefan Institute (IJS), Ljubljana, SI
subtitle: "Department of Knowledge Technologies"
summary: IJS is the leading Slovenian research institute for natural sciences with the KT Dept. performing research in advanced information technologies.

share: false
section_pager: true

authors:
- Tomaž Erjavec
- Nikola Ljubešić

type: ckcmc_partner
---

[Jožef Stefan Institute](https://www.ijs.si/ijsw/V001/JSI) is the
leading research institution for natural sciences in Slovenia having
over 900 researchers within 25 departments working in the areas of
computer science, physics, and chemistry and biology.

The [Department of Knowledge Technologies](http://kt.ijs.si/) has a
staff of 35 researchers and 15 PhD students and external
collaborators. The Department performs research in advanced
information technologies aimed at acquiring, storing and managing
knowledge to be used in the development of an information- and
knowledge-based society. Established areas of our work include
intelligent data analysis (machine learning, data mining, and
knowledge discovery in databases), computational creativity, decision
support and knowledge management.

The Department is also a recognised centre of research on language
technologies, computational linguistics, corpus linguistic, and
digital humanities. It has been involved in the compilation of the
majority of existing Slovene mono- and multilingual corpora, the
development of manually annotated corpora for training language
annotation tools, the development of the tools, such as part-of-speech
taggers, lemmatisers, parsers and named entity recognisers and in work
on standardisation of linguistic encoding in the scope of TEI and
ISO. The department is also the home of the Slovene research
infrastructure CLARIN.SI, a member of the CLARIN ERIC.
