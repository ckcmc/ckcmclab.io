---
title: CLARIN K(nowledge)-Centre for CMC Documentation
linktitle: CKCMC Docs
summary: The page for the CLARIN K(noledge)-Centre for CMC Documentation
type: book

toc: false
---

We offer expertise on language resources and technologies for Computer-Mediated
Communication and Social Media Corpora.  We can be contacted via [email to
`helpdesk @ THIS DOMAIN`](mailto:{{< param helpdesk_email >}}).  There, we
offer additional clarifications regarding this documentation and support in
using, modifying, producing, or publishing CMC resources and technologies.  For
more general questions, also consider joining the [European group of
researchers interested in the development and processing of CMC corpora @
Google Groups](https://groups.google.com/forum/?hl=de#!forum/cmc-corpora).

As we are a young K-centre, our documentation is still under development.  Once
we have collected enough input, we will be adding a FAQ section with your most
pressing questions.  We are also interested in your thoughts on missing or
wrong information.

In the meantime, we have already started to summarise important information for
the two central topics of our K-centre:
* [FAIR]({{< relref "/ckcmc/docs/fair/" >}}) best practices when creating a new CMC corpus
* [CMC-TEI]({{< relref "/ckcmc/docs/tei/" >}}) about a TEI standard for the representation of
  the structural and linguistic peculiarities of CMC
