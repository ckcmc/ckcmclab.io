---
title: Privacy Policy
share: false
commentable: false
editable: false

# Optional header image (relative to `static/media/` folder).
header:
  caption: ""
  image: ""
---
We take the necessary measures to apply the European and the applicable
national legislation on the subject of privacy with regard to the processing of
personal data of users when using the online web services of `cmc-corpora.org`.

The policy described herein applies solely to the websites and the web services
of `cmc-corpora.org` and excludes all other websites that can be accessed via
links that appear on the websites of `cmc-corpora.org`. 

Pursuant to legal provisions, the processing of personal data will be performed
in consideration of fundamental rights and freedoms as well as the dignity of
the data subject, and in accordance with the legislative provisions of the
Applicable Law and the confidentiality clauses included therein. In particular,
the processing of personal data will be carried out in accordance with the
principles of lawfulness, fairness, transparency, accuracy, purpose and storage
limitations, data minimisation, integrity and confidentiality.

Before using the websites of `cmc-corpora.org`, providing any personal data or
completing an electronic online form, you are invited to carefully read this
privacy policy.

The content of this privacy policy may be amended or updated to conform to
legal and regulatory obligations with respect to data protection, to allow for
technological advances on the webpage that could impact the modalities of
processing and to reflect organisational modifications that affect the privacy
structure. This privacy policy was last updated on {{< lastmod >}}.

## Relevant legal provisions
* [EU Data Protection Regulation 2016/679 (GDPR)](https://eur-lex.europa.eu/legal-content/EN/TXT/?uri=CELEX:32016R0679)
* [EU Recommendation 2/2001 on certain minimum requirements for collecting personal data on-line in the European Union](https://ec.europa.eu/justice/article-29/documentation/opinion-recommendation/files/2001/wp43_en.pdf)
* [Italian Legislative Decree No. 196/2003, integrated with the amendments Legislative Decree No 101/2018](https://www.garanteprivacy.it/documents/10160/0/Data+Protection+Code.pdf/7f4dc718-98e4-1af5-fb44-16a313f4e70f?version=1.3)


## Data protection information
The `cmc-corpora.org`-website is administered by [Eurac
Research](http://www.eurac.edu) staff and hosted on infrastructure from
[GitLab](https://gitlab.com/).  As such, the following privacy statements are
relevant:
* [Eurac Research Privacy Policy](http://www.eurac.edu/privacy/)
* [GitLab Privacy Policy](https://about.gitlab.com/privacy/)

## Rights of data subjects
All data subjects whose personal data are processed by online web services of
`cmc-corpora.org` may submit a request to exercise their rights pursuant to
Article 15 and following of the GDPR.

All requests from data subjects to exercise their rights may be addressed to
the data controller by e-mail to `privacy AT domain-name`, enclosing a copy of
an identification document and any other documentation deemed necessary. 
