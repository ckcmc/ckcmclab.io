---
# Documentation: https://wowchemy.com/docs/managing-content/

title: Proceedings of the 10th Web as Corpus Workshop (WAC-X) and the EmpiriST Shared
  Task
subtitle: ''
summary: ''
authors:
- Paul Cook
- Stefan Evert
- Roland Schäfer
- Egon Stemle
tags: []
categories: []
date: '2016-08-01'
lastmod: 2021-10-18T14:54:15+02:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2021-10-18T12:54:15.244669Z'
publication_types:
- '5'
abstract: The World Wide Web has become increasingly popular as a source of linguistic
  data, not only within the NLP communities, but also with theoretical linguists facing
  problems of data sparseness or data diversity. Accordingly, web corpora continue
  to gain importance, given their size and diversity in terms of genres/text types.
  The field is still new, though, and a number of issues in web corpus construction
  ne ed much additional research, both fundamental and applied. These issues range
  from questions of corpus design (e.g., assessment of corpus composition, sampling
  strategies and their relation to crawling algorithms , and handling of duplicated
  material) to more technical aspects (e.g., efficient implementation of individual
  post-processing steps in document cleaning and linguistic annotation, or large-scale
  parallelization to achieve web-scale corpus construction). Similarly, the systematic
  evaluation of web corpora, for example in the form of task based comparisons to
  traditional corpora, has only recently shifted into focus. For almost a decade,
  the ACL SIGWAC (http://www.sigwac.org.uk/), and especially the highly successful
  Web as Corpus (WAC) workshops have served as a platform for researchers interested
  in compilation, processing and application of web-derived corpora. Past workshops
  were co-located with major conferences on computational linguistics and/or corpus
  linguistics (such as EACL, NAACL, LREC, WWW, and Corpus Linguistics). WAC-X als
  o featured the final workshop of the EmpiriST 2015 shared task \"Automatic Linguistic
  Annotation of Computer-Mediated Communication / Social Media\" (see https://sites.google.com/site/empirist2015/
  for details) and the panel discussion \"Corpora, open science, and copyright reforms\"
  (see https://www.sigwac.org.uk/wiki/WAC-X#paneldisc for details).
publication: '*Association for Computational Linguistics*'
url_pdf: http://aclweb.org/anthology/W16-26
---
