---
# Documentation: https://wowchemy.com/docs/managing-content/

title: CMC Corpora through the prism of digital humanities
subtitle: ''
summary: ''
authors:
- Julien Longhi
- Claudia Marinica
profile: false
tags: []
categories: []
date: '2020-01-01'
lastmod: 2020-10-30T10:18:25+01:00
featured: true
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: [cmc-corpora2019]
publishDate: '2020-10-30T09:21:48.219043Z'
publication_types:
- '5'
abstract: This volume contains a selection of the papers presented at the 7th edition
  of the annual conference series on CMC and Social Media Corpora for the Humanities
  (CMC-Corpora2019) which was held at IDHN (Institute of digital humanities) on September,
  9th and 10th 2019, in Cergy-Pontoise, France.This volume contains papers which are
  related to the interdisciplinary perspective of digital humanities highlighted during
  this edition. The contributions in this book cover a wide range of both topics and
  languages, seen as contributions of different disciplines to the larger scope of
  digital humanities, as they articulate quantitative and qualitative approaches,
  formal and computational models, and specific analysis and more general conclusions
  for humanities.
publication: "*l'Harmattan*"
links:
- name: Publisher
  url: https://www.editions-harmattan.fr/index.asp?navig=catalogue&obj=livre&no=65876
---
