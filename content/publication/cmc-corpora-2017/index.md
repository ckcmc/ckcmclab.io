---
# Documentation: https://wowchemy.com/docs/managing-content/

title: Proceedings of the 5th Conference on CMC and Social Media Corpora for the Humanities
subtitle: ''
summary: ''
authors:
- Egon W. Stemle
- Ciara R. Wigham
profile: false
tags: []
categories:
- cmc-corpora conference proceeding
date: '2017-10-01'
lastmod: 2020-10-30T10:09:28+01:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: [cmc-corpora2017]
publishDate: '2020-10-30T09:09:28.636756Z'
publication_types:
- '9'
abstract: 'This volume presents the proceedings of the 5th edition of the annual conference
  series on CMC and Social Media Corpora for the Humanities (cmc-corpora2017). This
  conference series is dedicated to the collection, annotation, processing, and exploitation
  of corpora of computer-mediated communication (CMC) and social media for research
  in the humanities. The annual event brings together language-centered research on
  CMC and social media in linguistics, philologies, communication sciences, media
  and social sciences with research questions from the fields of corpus and computational
  linguistics, language technology, text technology, and machine learning. The 5th
  Conference on CMC and Social Media Corpora for the Humanities was held at Eurac
  Research on October, 4th and 5th, in Bolzano, Italy. This volume contains extended
  abstracts of the invited talks, papers, and extended abstracts of posters presented
  at the event. The conference attracted 26 valid submissions. Each submission was
  reviewed by at least two members of the scientific committee. This committee decided
  to accept 16 papers and 8 posters of which 14 papers and 3 posters were presented
  at the conference. The programme also includes three invited talks: two keynote
  talks by Aivars Glaznieks (Eurac Research, Italy) and A. Seza Doğruöz (Independent
  researcher) and an invited talk on the Common Language Resources and Technology
  Infrastructure (CLARIN) given by Darja Fišer, the CLARIN ERIC Director of User Involvement.'
publication: ''
url_pdf: https://zenodo.org/record/1040875/files/cmccorpora17-proceedings-v2.pdf
doi: 10.5281/zenodo.1040875
---
