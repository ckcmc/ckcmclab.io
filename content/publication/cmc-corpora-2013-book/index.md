---
# Documentation: https://wowchemy.com/docs/managing-content/

title: 'Building and annotating corpora of computer-mediated communication: Issues
  and challenges at the interface of corpus and computational linguistics'
subtitle: ''
summary: ''
authors:
- Michael Beißwenger
- Nelleke Ootdijk
- Angelika Storrer
- Henk van den Heuvel
profile: false
tags: []
categories: []
date: '2014-01-01'
lastmod: 2021-10-15T20:19:24+02:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: [cmc-corpora2013]
publishDate: '2021-10-15T18:19:24.245840Z'
publication_types:
- '5'
abstract: 'This special issue of the JLCL gathers five contributions of
  scholars and projects who aim to close the “CMC gap” in the corpora landscape
  for several European languages: the French CoMeRe project (Chanier et al.),
  the South Tyrolian DiDi project (Glaznieks & Stemle), the German Wikipedia
  corpus (Margaretha & Lüngen), the Dutch VU Chat corpus (Spooren & van
  Charldorp), and the research on language variation in Dutch Twitter data (van
  Halteren & Oostdijk).'
publication: '*Gesellschaft für Sprachtechnologie und Computerlinguistik (GSCL)*'
url_pdf: https://jlcl.org/issue/view/24/23
links:
- name: Publisher
  url: https://jlcl.org/issue/view/24
---
