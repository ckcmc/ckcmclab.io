---
# Documentation: https://wowchemy.com/docs/managing-content/

title: Proceedings of the 6th Conference on Computer-Mediated Communication (CMC)
  and Social Media (CMC-corpora 2018)
subtitle: ''
summary: ''
authors:
- Reinhild Vandekerckhove
- Darja Fišer
- Lisa Hilte
profile: false
tags: []
categories:
- cmc-corpora conference proceeding
date: '2018-01-01'
lastmod: 2020-10-30T10:09:28+01:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: [cmc-corpora2018]
publishDate: '2020-10-30T09:09:28.497561Z'
publication_types:
- '9'
abstract: Proceedings of the 6th Conference on Computer-Mediated Communication (CMC)
  and Social Media Corpora (CMC-corpora 2018), 17-18 September 2018, University of
  Antwerp
publication: '*Univerity of Antwerp*'
url_pdf: https://repository.uantwerpen.be/docman/irua/de0576/153416.pdf
links:
  - name: PID (HDL.NET)
    url: https://hdl.handle.net/10067/1534160151162165141
---
