---
# Documentation: https://wowchemy.com/docs/managing-content/

title: Investigating computer-mediated communication 
subtitle: 'Corpus-based approaches to language in the digital world'
summary: ''
authors:
- Darja Fišer
- Michael Beißwenger
profile: false
tags: []
categories: []
date: '2017-01-01'
lastmod: 2020-11-18T21:52:32+01:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: [cmc-corpora2016]
publishDate: '2020-11-18T20:52:31.959342Z'
publication_types:
- '5'
abstract: 'This volume brings together researchers active in the initiative called
  Computer-Mediated Communication and Social Media Corpora for the Humanities (http://www.cmc-corpora.org/)
  that is dedicated to the discussion of best practices on all aspects of open issues
  regarding the development, annotation, processing and analysis of corpora of computer-mediated
  communication (CMC). It includes eight chapters that have been written by 16 authors
  from 13 different countries and deal with the creation of CMC corpora, and with
  the analysis of CMC phenomena in 10 different languages. They tackle a diverse range
  of research questions and use a rich set of approaches, which is why they are organized
  into four broad thematic and methodological parts: Part 1 - Lexical analysis of
  CMC, Part 2 - Sociolinguistic analysis of CMC, Part 3 - Conversation and conflict
  in CMC, and Part 4 - Building and processing CMC resources.'
publication: '*Ljubljana University Press, Faculty of Arts*'
url_pdf: https://e-knjige.ff.uni-lj.si/znanstvena-zalozba/catalog/view/4/2/9-1
---
