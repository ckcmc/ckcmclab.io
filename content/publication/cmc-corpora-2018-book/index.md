---
# Documentation: https://wowchemy.com/docs/managing-content/

title: 'Special Issue: Computer-mediated communication (CMC) and social media corpora'
subtitle: ''
summary: ''
authors:
- Reinhild Vandekerckhove
- Lisa Hilte
- Darja Fišer
- Walter Daelemans
profile: false
tags: []
categories: []
date: '2019-09-01'
lastmod: 2020-12-02T10:51:18+01:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: [cmc-corpora2018]
publishDate: '2020-12-02T09:51:18.586799Z'
publication_types:
- '5'
abstract: 'This issue brings together language-centered studies on computer-mediated communication (CMC) and social media corpora. Except for Hilte et al., the papers in this thematic issue represent a selection of the papers presented at the 6th Conference on Computer-Mediated Communication and Social Media Corpora, 17th-18th September 2018, University of Antwerp (Belgium). The annual conference series is dedicated to the collection, annotation, processing and exploitation of corpora of computer-mediated communication (CMC) and social media for research in the humanities.'
publication: '*European Journal of Applied Linguistics*'
links:
- name: Publisher
  url: https://www.degruyter.com/view/journals/eujal/7/2/eujal.7.issue-2.xml
---
