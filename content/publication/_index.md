---
title: Publications from Conferences & Workshops, CKCMC, and CMC-TEI
cms_exclude: true

url: /publications

aliases:
- /publication/
# - /categories/publication/
# this probably should work... but doesn't. 
# hence, `/static/categoires/publication/index.html` exists and 
# redirects to `/publications/`

# View.
#   1 = List
#   2 = Compact
#   3 = Card
#   4 = Citation
view: 2

# Optional header image (relative to `static/media/` folder).
header:
  caption: ""
  image: ""
---
