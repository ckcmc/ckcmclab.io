---
# Documentation: https://wowchemy.com/docs/managing-content/

title: "Introducing the CLARIN K(nowledge)-Centre for CMC and Social Media Corpora (CKCMC)"
subtitle: ''
abstract: 'The CLARIN Knowledge Centre for Computer-Mediated Communication and Social Media Corpora (CKCMC) offers expertise on language resources and technologies for ComputerMediated Communication and Social Media. Its basic activities are to A) Give researchers, students, and other interested parties information about the available resources, technologies, and community activities, B) Support interested parties in producing, modifying or publishing relevant resources and technologies and C) Organise training activities. 

Additionally, the CKCMC manages the cmc-corpora.org website and helps in curating the CLARIN CMC Resource Family. The CKCMC can be reached via a Helpdesk (e-Mail). In this talk, we first want to introduce the CKCMC to the CMC-community; secondly, we would like to discuss with and get input from the CMC community on how to better serve our community goals or what further goals to pursue.'
authors:
- Egon W. Stemle
- Jennifer-Carmen Frey 
- Alexander König 
- Achille Falaise
- Tomaž Erjavec
- Harald Lüngen
profile: false
tags: []
categories:
- ckcmc 
- publication
date: '2022-10-06'
lastmod: 2022-10-06T14:54:17+02:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: [cmc-corpora2022]
publishDate: '2022-10-06'
publication_types:
- '1'
publication: ''
---
