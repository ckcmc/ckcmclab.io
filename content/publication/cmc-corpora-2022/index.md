---
# Documentation: https://wowchemy.com/docs/managing-content/

title: Book of Abstracts of the 9th Conference on Computer-Mediated Communication
  (CMC) and Social Media Corpora (CMC2022)
subtitle: ''
summary: ''
authors: []
tags: []
categories:
- cmc-corpora conference proceeding
date: '2022-10-06'
lastmod: 2022-10-06T14:54:17+02:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: [cmc-corpora2022]
publishDate: '2022-10-06T12:54:08.661288Z'
publication_types:
- '9'
abstract: ''
publication: '*University of Santiago de Compostela*'
url_pdf: https://www.usc.es/export9/sites/webinstitucional/en/congresos/cmc2022/download/Bookofabstracts.pdf
---
