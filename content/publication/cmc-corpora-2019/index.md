---
draft: false
abstract: "This volume presents the proceedings of the 7th edition of the annual
  conference series on CMC and Social Media Corpora for the Humanities
  (CMC-Corpora2019). This conference series is dedicated to the collection,
  annotation, processing, and exploitation of corpora of computer-mediated
  communication (CMC) and social media for research in the humanities. The
  annual event brings together language-centered research on CMC and social
  media in linguistics, philologies, communication sciences, media and social
  sciences with research questions from the fields of corpus and computational
  linguistics, language technology, text technology, and machine learning. The
  7th Conference on CMC and Social Media Corpora for the Humanities was held at
  IDHN (Institute of Digital Humanities) on September 9th and 10th, in
  Cergy-Pontoise, France. This volume contains extended abstracts of the papers
  (14), and abstracts of posters presented at the event (4). The program also
  includes two invited talks: one keynote talk by Marty Laforest (Université du
  Québec à Trois-Rivières, Canada), and one by Julien Velcin (University Lumière
  Lyon 2). The contributions in these proceedings cover a wide range of both
  topics and languages. Some contributions focus on standards and best practices
  of CMC corpora, others on the pragmatics of CMC, others on geographic
  linguistic variation, or applied linguistics, with discursive, semantic, or
  computational point of views."
url_pdf: https://cmccorpora19.sciencesconf.org/data/pages/proceedingsCMC_Corpora_2020.pdf
publication_types:
  - "9"
summary: ""
authors:
  - Julien Longhi
  - Claudia Marinica
profile: false
lastmod: 2020-10-30T10:09:28+01:00
publication: ""
featured: false
title: Proceedings of the 7th Conference on CMC and Social Media Corpora for the
  Humanities (CMC-Corpora2019)
subtitle: " "
date: 2019-09-01
tags: []
projects:
  - cmc-corpora2019
image:
  caption: ""
  focal_point: ""
  preview_only: false
categories:
  - cmc-corpora conference proceeding
publishDate: 2020-10-30T09:09:28.349041Z
---
