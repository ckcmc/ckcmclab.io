---
# Documentation: https://wowchemy.com/docs/managing-content/

title: 'CMC-core: a schema for the representation of CMC corpora in TEI'
subtitle: ''
summary: ''
authors:
- Michael Beißwenger
- Harald Lüngen
profile: false
tags: []
categories: []
date: '2020-01-01'
lastmod: 2020-11-18T21:35:05+01:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2020-11-18T20:35:05.701487Z'
publication_types:
- '2'
abstract: In this Paper, we describe a schema and models which have been developed
  for the representation of corpora of computer-mediated communicatin (CMC corpora)
  using the representation framework provided by the Text Encoding Initiative (TEI).
  We characterise CMC discourse as dialogic, sequentially organised interchange between
  humans and point out that many features of CMC are not adequately handled by current
  corpus encoding schemas and tools. We formulate desiderata for a representation
  of CMC in encoding schemes and argue why the TEI is a suitable framework for the
  encoding of CMC corpora. We propose a model of basic CMC units (utterances, posts,
  and nonverbal activities) and the macro- and micro-level structures of interactions
  in CMC environments. Based on these models, we introduce CMC-core, a TEI customisation
  for the encoding of CMC corpora, which defines CMC-specific encoding features on
  the four levels of elements, model classes, attribute classes, and modules of the
  TEI infrastructure. The description of our customisation is illustrated by encoding
  examples from corpora by researchers of the TEI SIG CMC, representing a variety
  of CMC genres, i.e. chat, wiki talk, twitter, blog, and Second Life interactions.
  The material described, i.e. schemata, encoding examples, and documentation, is
  available from the of the TEI CMC SIG Wiki and will accompany a feature request
  to the TEI council in late 2019.
publication: '*Corpus*'
url_pdf: http://journals.openedition.org/corpus/4553
doi: 10.4000/corpus.4553
---
