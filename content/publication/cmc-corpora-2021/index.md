---
# Documentation: https://wowchemy.com/docs/managing-content/

title: Proceedings of the 8th Conference on Computer-Mediated Communication CMC and
  Social Media Corpora (CMC-Corpora2021)
subtitle: ''
summary: ''
authors:
- Iris Hendrickx
- Lieke Verheijen
- Lidwien van de Wijngaert
tags: []
categories:
- cmc-corpora conference proceeding
date: '2021-10-01'
lastmod: 2021-11-01T01:01:33+01:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: [cmc-corpora2021]
publishDate: '2021-11-01T00:01:33.568081Z'
publication_types:
- '9'
abstract: After having to postpone the 2020 edition of the Conference on Computer-Mediated
  Communication (CMC) and Social Media Corpora because of the global COVID-19 pandemic,
  we are very pleased to present the proceedings of the 8th edition of the conference
  (CMC-corpora 2021). This conference series is dedicated to the collection, annotation,
  processing, analysis, and exploitation of corpora of computer-mediated communication
  and social media for research in the humanities and beyond. The annual event brings
  together language-centred research on CMC and social media in linguistics, communication
  sciences, media studies, and social sciences with research questions from the fields
  of corpus and computational linguistics, language and text technology, and machine
  learning.
publication: '*Radboud University*'
# url_pdf: https://surfdrive.surf.nl/files/index.php/s/Lcgx6d3EwGMjugR
---
