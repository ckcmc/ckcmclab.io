---
# Documentation: https://wowchemy.com/docs/managing-content/

title: The FAIR Guiding Principles for scientific data management and stewardship
subtitle: ''
summary: ''
authors:
- Mark D. Wilkinson
- Michel Dumontier
- IJsbrand Jan Aalbersberg
- Gabrielle Appleton
- Myles Axton
- Arie Baak
- Niklas Blomberg
- Jan-Willem Boiten
- Luiz Bonino da Silva Santos
- Philip E. Bourne
- Jildau Bouwman
- Anthony J. Brookes
- Tim Clark
- Mercè Crosas
- Ingrid Dillo
- Olivier Dumon
- Scott Edmunds
- Chris T. Evelo
- Richard Finkers
- Alejandra Gonzalez-Beltran
- Alasdair J.G. Gray
- Paul Groth
- Carole Goble
- Jeffrey S. Grethe
- Jaap Heringa
- Peter A.C ’t Hoen
- Rob Hooft
- Tobias Kuhn
- Ruben Kok
- Joost Kok
- Scott J. Lusher
- Maryann E. Martone
- Albert Mons
- Abel L. Packer
- Bengt Persson
- Philippe Rocca-Serra
- Marco Roos
- Rene van Schaik
- Susanna-Assunta Sansone
- Erik Schultes
- Thierry Sengstag
- Ted Slater
- George Strawn
- Morris A. Swertz
- Mark Thompson
- Johan van der Lei
- Erik van Mulligen
- Jan Velterop
- Andra Waagmeester
- Peter Wittenburg
- Katherine Wolstencroft
- Jun Zhao
- Barend Mons
profile: false
tags: []
categories: []
date: '2016-03-01'
lastmod: 2020-11-18T21:35:06+01:00
featured: false
draft: false
share: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2020-11-18T20:35:05.936374Z'
publication_types:
- '2'
abstract: 'There is an urgent need to improve the infrastructure supporting the
  reuse of scholarly data. A diverse set of stakeholders—representing academia,
  industry, funding agencies, and scholarly publishers—have come together to
  design and jointly endorse a concise and measureable set of principles that we
  refer to as the FAIR Data Principles. The intent is that these may act as a
  guideline for those wishing to enhance the reusability of their data holdings.
  Distinct from peer initiatives that focus on the human scholar, the FAIR
  Principles put specific emphasis on enhancing the ability of machines to
  automatically find and use the data, in addition to supporting its reuse by
  individuals. This Comment is the first formal publication of the FAIR
  Principles, and includes the rationale behind them, and some exemplar
  implementations in the community.'
publication: '*Scientific Data*'
url_pdf: http://www.nature.com/articles/sdata201618
doi: 10.1038/sdata.2016.18
---
