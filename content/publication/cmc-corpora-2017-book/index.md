---
# Documentation: https://wowchemy.com/docs/managing-content/

title: Building Computer-Mediated Communication Corpora for sociolinguistic Analysis
subtitle: ''
summary: ''
authors:
- Ciara R. Wigham
- Egon W. Stemle
profile: false
tags: []
categories: []
date: '2019-06-01'
lastmod: 2020-11-18T21:36:28+01:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: [cmc-corpora2017]
publishDate: '2020-11-18T20:36:28.222346Z'
publication_types:
- '5'
abstract: Communication between humans via networked devices has become an everyday
  part of people's lives across generations, cultures, geographical areas, and social
  classes. Shaped by the specific social and technical context in which it is produced,
  synchronous and asynchronous computer-mediated communication (CMC) has become increasingly
  participatory, interactive, and multimodal. User interactions and user-generated
  social media content offer a wide range of research opportunities for a growing
  multidisciplinary research community. This edited volume combines methodological
  papers that focus on building and annotating CMC corpora and papers that offer a
  sociolinguistic analysis of different CMC corpora. The diversity of languages represented
  in the corpora include Arabic, French, German, Italian, English and Slovenian. In
  fact, the increasingly multilingual nature of CMC data is a recurring theme throughout
  the volume, as are the references to the importance of and compliance with standards
  for CMC corpora development in order to facilitate (the?) re-examination of corpora
  for reproducibility, and for other areas and objectives of investigation. All but
  one paper are extended papers from the 2017 edition of the CMC and Social Media
  Corpora Conference held in Bolzano, Italy where the community met to discuss themes
  that related to the interaction between language, CMC, and society.
publication: '*Presses Universitaires Blaise Pascal*'
links:
- name: Publisher
  url: http://pubp.giantchair.com/livre/?GCOI=28451100141150&language=en
---
