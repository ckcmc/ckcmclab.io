---
title: Julien Longhi
bio: Professor of Linguistics
avatar_filename: avatar
social:
  - icon: twitter
    icon_pack: fab
    link: https://twitter.com/jlonghi1

email: julien.longhi@cyu.fr
superuser: false

# Organizations/Affiliations to show in About widget
organizations:
- name: Université de Cergy-Pontoise
  url: https://www.cyu.fr/m-julien-longhi


user_groups:
  - steering committee
committee_period: since 2021
highlight_name: false
---
Julien is Professor of Linguistics at CY Cergy Paris Université. He specializes
in discourse analysis of political and media texts, with a particular focus on
ideologies, social media and digital humanities. He has published books,
articles and edited volumes in the fields of pragmatics, semantics and corpus
linguistics, in addition to discourse analysis.  He is the director of the
institute of digital humanities of CY Cergy Paris Université. 
