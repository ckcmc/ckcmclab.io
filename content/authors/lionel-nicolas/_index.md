---
title: Lionel Nicolas
bio: Senior Researcher
social: []
interests:
  - Computational linguistics
  - Natural language processing
  - Computer science
  - Crowdsourcing
  - Language learning
organizations:
  - name: Institute for Applied Linguistics
    url: http://www.eurac.edu/linguistics
  - name: Eurac Research
    url: http://www.eurac.edu/
email: lionel.nicolas@eurac.edu
superuser: false
user_groups:
  - CKCMC
  - Eurac Research
highlight_name: false
---
