---
title: Steven Coats
bio: ""
interests:
  - language variation
  - online language and social media
  - computational approaches to language analysis
organizations:
  - name: University of Oulu, Faculty of Humanities
    url: http://cc.oulu.fi/~scoats/
email: steven.coats@oulu.fi
superuser: false
user_groups:
  - steering committee
committee_period: since 2018
highlight_name: false
---
Steven Coats is a lecturer in English at the University of Oulu, Finland, interested in corpus approaches to computer-mediated communication, sociolinguistics, and dialectology. He has published articles on topics such as Twitter discourse, skin tone emoji, verbal Anglicisms in German, and corpus creation from YouTube videos, and created the 1.25-billion-word *Corpus of North American Spoken English*, a publicly-available resource for research into language variation.
