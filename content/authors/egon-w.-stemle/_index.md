---
title: Egon W. Stemle
bio: Researcher in Applied Linguistics
social:
  - icon: globe-europe
    icon_pack: fas
    link: https://iiegn.eu
  - icon: twitter
    icon_pack: fab
    link: https://twitter.com/iiegn
interests:
  - Artificial Intelligence
  - Computational Linguistics
  - Cognitive Science
organizations:
  - name: Institute for Applied Linguistics
    url: http://www.eurac.edu/linguistics
  - name: Eurac Research
    url: http://www.eurac.edu/
superuser: false
user_groups:
  - Eurac Research
  - CKCMC
highlight_name: false
email: egon.stemle@eurac.edu
---
Egon works on the creation, standardisation, and interoperability of tools for
editing, processing, and annotating linguistic data. He collects and processes
data from the Web, from social media, and language learners and also enjoys
working together with other scientists on their data. He develops tools that
can apply our everyday understanding of language to texts, be it to help
teachers to correct texts, students to write texts or just interested people
who search for content in texts that elude a simple Google search. His
curiosity in research is driven by the question of why people can easily find
meaning in texts, even at a young age, while automatic processes are often of
little use or fail completely.

He is an advocate of open science to make research and data available for
others to consult or reuse in new research, and he is the leader of this CLARIN
K-centre for CMC.
