---
title: Aivars Glaznieks
bio: Senior Researcher
social:
  - icon: twitter
    icon_pack: fab
    link: https://twitter.com/aivarsglaznieks
interests:
  - Writing research
  - Language acquisition
  - Computer-mediated communication
  - Corpus linguistics
  - Sociolinguistics
organizations:
  - name: Institute for Applied Linguistics
    url: http://www.eurac.edu/linguistics
  - name: Eurac Research
    url: http://www.eurac.edu/
email: aivars.glaznieks@eurac.edu
superuser: false
user_groups:
  - CKCMC
  - Eurac Research
highlight_name: false
---
Aivars is a senior researcher at the Institute for Applied Linguistics of Eurac
Research Bozen/Bolzano. His research focuses on sociolinguistic aspects of the
use of registers in CMC. He has been responsible for the creation of the
[multilingual CMC corpus DiDi](http://hdl.handle.net/20.500.12124/7), a corpus
linguistic collection of facebook texts of writers from the multilingual
Italian province of South Tyrol. Currently, he is working on the description of
written German dialect varieties in CMC. As he always needs more authentic data
for his work, he is happy to collect new data whenever possible in order to
produce and analyse new corpora!

He has worked on the creation of several learner corpora in recent years, such
as [LEONIDE](http://hdl.handle.net/20.500.12124/25) and
[KoKo](http://hdl.handle.net/20.500.12124/12), and combines writing research
and corpus linguistics in his research activities.

Aivars is also the managing editor for the new [Eurac Research Learner Corpus
Platform PORTA](https://porta.eurac.edu).
