---
# Display name
title: Andreas Witt

# Is this the primary user of the site?
superuser: false

# Organizations/Affiliations to show in About widget
organizations:
- name: IDS
  url: https://www.ids-mannheim.de

# Short bio (displayed in user profile at end of posts)
bio: Head of the Digital Linguistics Department

social:
- link: https://www.ids-mannheim.de/digspra/personal/witt/
  icon: globe-europe
  icon_pack: fas


# Interests to show in About widget
interests:
- Text technology
- Digital Humanities
- Information Modelling
- Corpus Linguistics
- Markup Languages

# Link to a PDF of your resume/CV.
# To use: copy your resume to `static/media/resume.pdf`, enable `ai` icons in `params.toml`, 
# and uncomment the lines below.
# - icon: cv
#   icon_pack: ai
#   link: media/resume.pdf

# Email for Contact widget or Gravatar
email: "witt@ids-mannheim.de"

# Highlight the author in author lists? (true/false)
highlight_name: false

# Organizational groups that you belong to (for People widget)
#   Remove this if you are not using the People widget.
user_groups:
- IDS
- CKCMC 
---

Andreas Witt is Professor of Computational Humanities and Text
Technologies at the University of Mannheim and heads the department of
Digital Linguistics at the IDS. He is Honorary Professor of Digital
Humanities at the Institute for Computational Linguistics at
Heidelberg University and – by co-optation – Professor at the
Institute for Digital Humanities at the University of Cologne. Since
2020, he has been a member of the Board of Directors of
CLARIN-ERIC. He is a member of the scientific advisory boards of the
Meertens Instituut in Amsterdam and the Austrian Centre for Digital
Humanities in Vienna, and active in many national and international
research organizations, review panels and expert groups. Witt's
research is situated in the field of annotation science, he is Chair
of the ISO working group on linguistic annotation, and Co-Chair of the
Special Interest Group: TEI for Linguists. Witt also publishes on
legal and ethical issues that are pertaining to research.
