---
# Display name
title: Jennifer-Carmen Frey

# Is this the primary user of the site?
superuser: false

# Organizations/Affiliations to show in About widget
organizations:
  - name: Institute for Applied Linguistics
    url: http://www.eurac.edu/linguistics
  - name: Eurac Research
    url: http://www.eurac.edu/

# Short bio (displayed in user profile at end of posts)
bio: Post-Doc Researcher

# Interests to show in About widget
interests:
  - Corpus linguistics
  - Computational Linguistics
  - Literacy development and language learning and teaching
  - Non-standard language
  - Computer-mediated communication
  - Technology-enhanced learning and computer-assistend language learning and teaching

# Link to a PDF of your resume/CV.
# To use: copy your resume to `static/media/resume.pdf`, enable `ai` icons in `params.toml`, 
# and uncomment the lines below.
# - icon: cv
#   icon_pack: ai
#   link: media/resume.pdf

# Email for Contact widget or Gravatar
email: "jennifer.frey@eurac.edu"

# Highlight the author in author lists? (true/false)
highlight_name: false

# Organizational groups that you belong to (for People widget)
#   Remove this if you are not using the People widget.
user_groups:
- Eurac Research
- CKCMC 
---
Jennifer has been working on the creation and analysis of various cmc and other non-standard language corpora (e.g. learner corpora, corpora of student essays). She has gathered vast experience in technical, legal and methodological issues concerning the collection, creation, analysis and publication of cmc corpora, using her skills in computer science, AI and NLP as well as statistics. 
Her interdisciplinary approach, that originated in her study background, is complemented by broad interests in language varieties, literacy development, language learning and teaching, but also media literacy and technology-enhanced learning. After studying German language and computer science for a teacher's degree in Graz, Austria, she worked several years in corpus linguistic projects at the Institute for Applied Linguistics at Eurac Research and recently finished her PhD at the University of Bologna, where she investigated machine-learning approaches for the analysis of German non-standard corpora.
