---
title: Thomas Schmidt
social:
  - icon: globe-europe
    icon_pack: fas
    link: https://linguisticbits.de/#resume
superuser: false
highlight_name: false
---
