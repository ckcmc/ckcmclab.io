---
# Display name
title: Darja Fišer 
# Proxy for Home Page
social:
- icon: globe-europe
  icon_pack: fas
  link: http://lojze.lugos.si/darja/
- icon: twitter
  icon_pack: fab
  link: https://twitter.com/dfiser3

# Is this the primary user of the site?
superuser: false

# Organizations/Affiliations to show in About widget
organizations:
- name: University of Ljubljana, Faculty of Arts, Department of Translation
  url: https://www.ff.uni-lj.si/en/staff/darja-fiser
- name: Institute for Contemporay History
  url: https://www.inz.si/en/
- name: Jožef Stefan Institute
  url: http://kt.ijs.si/
- name: CLARIN ERIC
  url: https://www.clarin.eu/view-contact-nocountry/18921/1038
  
# Short bio (displayed in user profile at end of posts)
bio: Assistant Professor in Corpus and Computational Linguistics

# Interests to show in About widget
interests:
- non-standard language processing
- computational lexical semantics
- language resources for Slovene
- corpus linguistics
- translation technologies

# Link to a PDF of your resume/CV.
# To use: copy your resume to `static/media/resume.pdf`, enable `ai` icons in `params.toml`, 
# and uncomment the lines below.
# - icon: cv
#   icon_pack: ai
#   link: media/resume.pdf

# Email for Contact widget or Gravatar
email: "darja.fiser@ff.uni-lj.si"

# Highlight the author in author lists? (true/false)
highlight_name: false

# Organizational groups that you belong to (for People widget)
#   Remove this if you are not using the People widget.
user_groups:
- IJS
- CKCMC 
- former steering committee
committee_period: "2015 - 2019"

---
Darja is Associate Professor at the Faculty of Arts, University of
Ljubljana, and Senior Research Fellow at the Department of Knowledge
Technologies, Jožef Stefan Institute. She is active in the fields of
computer-mediated communication and lexical semantics using
corpus-linguistics methods and natural language processing.

She is currenlty the principal investigator of a bilateral research
project focused on the analysis of the linguistic landscape of hate
speech in social media (LiLaH), project member of an interdisciplinary
national basic research project on the resources, tools and methods
for the understanding, identification and classification of socially
unacceptable discourse in the information society (FRENK), and a
collaborator on the H2020 cluster project Social Science and
Humanities Open Cloud (SSHOC).

She is also a Board Member of the Slovenian Language Technologies
Society, Chair of the FoLLi Steering Committee of the biggest European
summer school on language, logic and computation ESSLLI, and is
currently serving as a member of the Scientific Advisory Board of the
Austrian Centre for Digital Humanities at the Austrian Academy of
Sciences and of the National Interdisciplinary Research
E-Infrastructure for Bulgarian Language and Cultural Heritage
Resources and Technologies.  She was the Vice Executive Director of
CLARIN ERIC (2018-2020), in which role she has strengthened the
visibility of CLARIN and has fostered knowledge sharing within
existing user communities. She also worked on widening the
disciplinary outreach and streamlining CLARIN’s functionality to meet
the requirements of researchers, educators and students.
