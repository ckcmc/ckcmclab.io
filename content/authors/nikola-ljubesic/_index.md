---
# Display name
title: Nikola Ljubešić
# Proxy for Home Page
social:
- icon: globe-europe
  icon_pack: fas
  link: https://nljubesi.github.io/
- icon: twitter
  icon_pack: fab
  link: https://twitter.com/nljubesic
# Interests to show in About widget
interests:
- Computational Linguistics
- Machine learning methods
- Processing of South Slavic languages
# Organizations/Affiliations to show in About widget
organizations:
- name: Jožef Stefan Institute
  url: http://kt.ijs.si/
- name: Faculty of Computer Science, Ljubljana University
  url: https://www.fri.uni-lj.si/en/laboratory/lkm
- name: CLASSLA K-centre
  url: https://www.clarin.si/info/k-centre/
# Email for Contact widget or Gravatar
email: "nikola.ljubesic@ijs.si"
superuser: false
user_groups:
- IJS
- CKCMC 

# Highlight the author in author lists? (true/false)
highlight_name: false

bio: Senior researcher

# Link to a PDF of your resume/CV.
# To use: copy your resume to `static/media/resume.pdf`, enable `ai` icons in `params.toml`, 
# and uncomment the lines below.
# - icon: cv
#   icon_pack: ai
#   link: media/resume.pdf

---

Nikola's research interests cover a broad spectrum of NLP problems
including web corpus construction, terminology extraction, bilingual
lexicon extraction and machine translation. He is an expert in machine
learning approaches, and has developed several methods for
word-normalisation, also applied to CMC corpora.

He is the leader of the CLARIN K-centre CLASSLA, for Computational Processing of South Slavic languages.
