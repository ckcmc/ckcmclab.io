---
# Display name
title: Achille Falaise

# Is this the primary user of the site?
superuser: false

# Organizations/Affiliations to show in About widget
organizations:
- name: Formal Linguistics Laboratory
  url: http://www.llf.cnrs.fr/

# Short bio (displayed in user profile at end of posts)
bio: Linguistic data engineer

# Interests to show in About widget
interests:
- Computational Linguistics for humans
- Corpus Linguistics

# Link to a PDF of your resume/CV.
# To use: copy your resume to `static/media/resume.pdf`, enable `ai` icons in `params.toml`, 
# and uncomment the lines below.
# - icon: cv
#   icon_pack: ai
#   link: media/resume.pdf

# Email for Contact widget or Gravatar
email: "afalaise@linguist.univ-paris-diderot.fr"

# Highlight the author in author lists? (true/false)
highlight_name: false

# Organizational groups that you belong to (for People widget)
#   Remove this if you are not using the People widget.
user_groups:
- LLF
- CKCMC 
---

Achille is a linguistic data engineer, mainly invested in corpora. He collects, formats, parses and annotates linguistic corpora; but he is also eager to provide tools that let all kind of language professionals access and use them easily, whether they are linguists, teachers or translators. He has a liking for corpora of spontaneous and "natural" writing, such as online chats, popular writing in general, but also classical texts older than normative grammars, which allow for the study the natural flow of language evolution across time.
