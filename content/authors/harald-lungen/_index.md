---
# Display name
title:  Harald Lüngen

# Is this the primary user of the site?
superuser: false

# Organizations/Affiliations to show in About widget
organizations:
- name: IDS
  url: https://www.ids-mannheim.de

bio: Researcher in the corpus linguistics programme area
social:
- link: https://www.ids-mannheim.de/digspra/personal/luengen/

# Interests to show in About widget
interests:
- Computational Linguistics
- Text and Corpus Technology

# Link to a PDF of your resume/CV.
# To use: copy your resume to `static/media/resume.pdf`, enable `ai` icons in `params.toml`, 
# and uncomment the lines below.
# - icon: cv
#   icon_pack: ai
#   link: media/resume.pdf

# Email for Contact widget or Gravatar
email: "luengen@ids-mannheim.de"

# Highlight the author in author lists? (true/false)
highlight_name: false

# Organizational groups that you belong to (for People widget)
#   Remove this if you are not using the People widget.
user_groups:
- IDS
- CKCMC 
---

Harald is a computational linguist and a member of the corpus building project for written corpora at the IDS. He works on the acquisition of text data and their preparation and integration in the German Reference Corpus DeReKo. He is responsible for I5, the TEI customisation used for encoding DeReKo. In his research, he is concerned with text and corpus technology and with issues of building and managing very large corpora. In particular, he is interested in the modelling and encoding of CMC corpora. He is a member of the TEI SIG CMC. 
