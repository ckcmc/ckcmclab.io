---
title: Lieke Verheijen
avatar_filename: avatar
social: []

interests:
  - online writing and social media
  - orthography/spelling
  - emoji

# Organizations/Affiliations to show in About widget
organizations:
- name: Radboud University, Department of Language and Communication
  url: https://www.ru.nl/english/people/verheijen-a/


email: lieke.verheijen@ru.nl


superuser: false

# Organizational groups that you belong to (for People widget)
#   Remove this if you are not using the People widget.
user_groups:
- steering committee

committee_period: "since 2021"

---
Lieke Verheijen is Assistant Professor at the department of Language and 
Communication at Radboud University in Nijmegen, the Netherlands. Her 
current research, conducted at the university’s Centre for Language Studies, 
is aimed at exploring emoji use and perceptions in personal and professional 
computer-mediated communication. She teaches courses about e.g. corporate 
communication, social media, and crisis communication and webcare. In addition, 
she is a member of the Spelling Committee, which advises on spelling policy and 
the official spelling of the Dutch language.
