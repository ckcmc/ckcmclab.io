---
title: Lisa Hilte
bio: Post-doc researcher
interests:
  - Sociolinguistics with a quantitative / computational focus
  - Computational Linguistics
organizations:
  - name: University of Antwerp, Computational Linguistics, Psycholinguistics and
      Sociolinguistics research center (CLiPS)
    url: https://www.uantwerpen.be/en/research-groups/clips/
email: lisa.hilte@uantwerpen.be
superuser: false
user_groups:
  - former steering committee
committee_period: 2019 - 2022
highlight_name: false
---

