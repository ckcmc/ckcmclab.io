---
# Display name
title: Tomaž Erjavec
# Proxy for Home Page
social:
- icon: globe-europe
  icon_pack: fas
  link: https://nl.ijs.si/et/
# Interests to show in About widget
interests:
- Language resources
- Standards for language encoding
- Digital Humanities
- Computational Linguistics
# Organizations/Affiliations to show in About widget
organizations:
- name: CLARIN.SI
  url: https://www.clarin.si/
- name: Jožef Stefan Institute
  url: http://kt.ijs.si/
- name: Institute for Slovenian language
  url: https://isjfr.zrc-sazu.si/en/predstavitev#v
email: "tomaz.erjavec@ijs.si"
superuser: false
user_groups:
- IJS
- CKCMC 

# Highlight the author in author lists? (true/false)
highlight_name: false

bio: Senior researcher

# Link to a PDF of your resume/CV.
# To use: copy your resume to `static/media/resume.pdf`, enable `ai` icons in `params.toml`, 
# and uncomment the lines below.
# - icon: cv
#   icon_pack: ai
#   link: media/resume.pdf

---
Tomaž Erjavec works at the Dept. of Knowledge Technologies at the
Jožef Stefan Institute and at the Institute of the Slovenian language
"Fran Ramovš" at the Scientific Research Centre of the Slovenian
Academy of Sciences and Arts.

His research interests lie in the field of language technologies, with
a focus on South Slavic languages, in particular Slovenian, and on
methods and standards for the compilation and annotation of language
resources.

His work on CMC comprises the compilation of the first large corpus of
Slovenian CMC and work on automatic standardisation of words.

He was the founding president of the Slovenian Language Technologies
Society, is a member of the Slovenian standards organisation in ISO TC
37 SC4 “Language resource management” and the national coordinator of
the CLARIN.SI research infrastructure.
