---
title: Alexander König
bio: 
social:
  - icon: twitter
    icon_pack: fab
    link: https://twitter.com/kreetrapper
interests: []
organizations:
  - name: CLARIN-ERIC
    url: https://clarin.eu
superuser: false
user_groups: []
highlight_name: false
email: alex@clarin.eu
---
