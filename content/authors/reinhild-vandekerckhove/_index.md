---
title: Reinhild Vandekerckhove
bio: Professor Dutch Linguistics & Sociolinguistics
interests:
  - Sociolinguistics
  - Dutch Linguistics
organizations:
  - name: University of Antwerp, Computational Linguistics, Psycholinguistics and
      Sociolinguistics research center (CLiPS)
    url: https://www.uantwerpen.be/en/research-groups/clips/
email: reinhild.vandekerckhove@uantwerpen.be
superuser: false
user_groups:
  - steering committee
committee_period: since 2022
highlight_name: false
---

