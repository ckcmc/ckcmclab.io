---
# Display name
title: Ciara Wigham

social:
  - icon: twitter
    icon_pack: fab
    link: https://twitter.com/loopy63

# Is this the primary user of the site?
superuser: false

# Organizations/Affiliations to show in About widget
organizations:
- name: Université Clermont Auvergne, LRL (Laboratoire de recherche sur le langage)

# Interests to show in About widget
interests: []

# Link to a PDF of your resume/CV.
# To use: copy your resume to `static/media/resume.pdf`, enable `ai` icons in `params.toml`, 
# and uncomment the lines below.
# - icon: cv
#   icon_pack: ai
#   link: media/resume.pdf

# Email for Contact widget or Gravatar
email: "ciara.wigham@uca.fr"

# Highlight the author in author lists? (true/false)
highlight_name: false

# Organizational groups that you belong to (for People widget)
#   Remove this if you are not using the People widget.
user_groups:
- former steering committee

committee_period: "2015 - 2018"

---
