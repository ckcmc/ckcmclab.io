---
# Display name
title: Thierry Chanier

# Is this the primary user of the site?
superuser: false

# Organizations/Affiliations to show in About widget
organizations:
- name: Université Blaise Pascal, Laboratoire de recherche sur le langage (LRL)

eMail: thierry.chanier @ univ-bpclermont.fr (without spaces)

# Short bio (displayed in user profile at end of posts)
#bio: Something about Thierry.

# Interests to show in About widget
interests: []

# Link to a PDF of your resume/CV.
# To use: copy your resume to `static/media/resume.pdf`, enable `ai` icons in `params.toml`, 
# and uncomment the lines below.
# - icon: cv
#   icon_pack: ai
#   link: media/resume.pdf

# Email for Contact widget or Gravatar
email: "thierry.chanier@univ-bpclermont.fr"

# Highlight the author in author lists? (true/false)
highlight_name: false

# Organizational groups that you belong to (for People widget)
#   Remove this if you are not using the People widget.
user_groups:
- former steering committee

committee_period: "2013 - 2015"
---
