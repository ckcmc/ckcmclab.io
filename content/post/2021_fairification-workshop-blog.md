---
title: "A Recap on the Workshop on Data Management for FAIR CMC Corpora" 
subtitle: "Workshop: Blog post"
summary: ""
authors:
- Egon W. Stemle
- Jennifer-Carmen Frey
- Alexander König
profile: false
tags: []
categories:
- workshop
- ckcmc
date: 2021-11-04
featured: true
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: [cmc-corpora2021]
---
A blog post by Jennifer-Carmen Frey, Alexander König, Egon W. Stemle about the virtual workshop on ‘Data Management for FAIR CMC Corpora'  @ CMC-Corpora 2021 is now online:

<https://www.clarin.eu/blog/recap-workshop-data-management-fair-cmc-corpora>

