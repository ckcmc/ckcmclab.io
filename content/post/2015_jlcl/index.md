---
# Documentation: https://wowchemy.com/docs/managing-content/

title: "JLCL special issue on building & annotating CMC corpora"
subtitle: "New Publication"
summary: ""
authors:
- Michael Beißwenger
profile: false
tags: []
categories:
- publication
date: 2015-12-05T11:48:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  preview_only: true
  caption: ""
  focal_point: ""

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
---
{{< cite page="/publication/cmc-corpora-2013-book" view="2" >}}
