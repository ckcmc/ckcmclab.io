---
title: "10th Conference on Computer-Mediated Communication (CMC) and Social Media Corpora"
subtitle: "Conference Announcement"
authors:
- Egon W. Stemle
profile: false
categories:
- conference
date: 2022-11-22
draft: false
featured: false
image:
  filename: featured
  focal_point: Smart
  preview_only: false
projects: [cmc-corpora2023]
---
Lo and behold, the impressive venue for CMC-Corpora 2023 (14-15 September) in Mannheim, Germany 🤩! Call for papers going out soon...
