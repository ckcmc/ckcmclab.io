---
title: "CLARIN UI Event: Using TEI for representing CMC/social media data" 
subtitle: "Workshop: Blog post"
summary: ""
authors: ""
profile: false
tags: []
categories:
- workshop
date: 2017-10-25
featured: true
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: [cmc-corpora2017]
---
A blog post by Daniel Pfurtscheller (Universität Innsbruck) about the CLARIN UI Event @ CMC-Corpora 2017 is now online:

<https://www.clarin.eu/news/clarin-ui-event-using-tei-representing-cmcsocial-media-data>

