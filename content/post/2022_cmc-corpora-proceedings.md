---
# Documentation: https://wowchemy.com/docs/managing-content/

title: "Book of Abstracts of the 9th Conference on Computer-Mediated Communication (CMC) and Social Media Corpora (CMC2022)"
subtitle: "New Publication"
authors:
- Egon W. Stemle
profile: false
tags: []
categories:
- publication
date: 2022-10-06
featured: false
draft: false
section_pager: true

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  preview_only: true
  caption: ""
  focal_point: ""

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: [cmc-corpora2022]

---

{{< cite page="/publication/cmc-corpora-2022" view="2" >}}
