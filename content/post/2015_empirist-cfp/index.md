---
title: "Shared Task on Processing German CMC/Social Media & Web Data"
subtitle: "Shared Task: Call for Participation"
authors:
- Michael Beißwenger
profile: false
tags: []
categories:
- call for participation
- shared task
date: 2015-12-21T19:36:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  preview_only: true
  caption: ""
  focal_point: ""

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []

links:
- name: Shared task website
  url: https://sites.google.com/site/empirist2015/
---

The EmpiriST 2015 shared task aims to encourage the developers of NLP
applications to adapt their tools and resources for the processing of written
German discourse in genres of computer-mediated communication (CMC) - such as
chats, forums, wiki talk pages, tweets, blog comments, social networks, SMS and
WhatsApp dialogues - as well as monological web pages - such as personal or
professional blogs, Wikipedia articles, academic sites, etc.

The shared task is divided into two subtasks (A: tokenization, B: POS tagging)
and two different data sets (CMC subset, web corpora subset). While our main
goal is to foster the development of robust tools that work well on a wide
range of CMC & web genres, teams are allowed to focus on one subtask or one
subset only. Full manually annotated training data are available now on the
EmpiriST homepage, comprising approx. 5000 tokens for each subset.

Results and system descriptions will be presented in the WAC-X workshop
co-located with ACL 2016 in Berlin, Germany (11 or 12 August 2016).

For more information, including detailed annotation guidelines and instructions
for participation, see the EmpiriST homepage at:
https://sites.google.com/site/empirist2015/

and join our Google group for updates, questions and discussion:
https://groups.google.com/d/forum/empirist2015

While EmpiriST is focussed on the annotation of German-language data,
familiarity with German is not essential for participating in the task. There
are sufficient amounts of training data for general machine learning, domain
adaptation and optimization approaches. We also provide an English summary of
the POS tagset and annotation guidelines.

## SCHEDULE:
* 20.12.2015        Release of the training data
* 31.01.2016        Team registration
* 15.02.2016        Release of the evaluation data for the tokenization subtask
* 19.02.2016        Submission deadline for the tokenization subtask
* 22.02.2016        Release of the evaluation data for the POS-tagging subtask
* 26.02.2016        Submission deadline for the POS-tagging subtask
* ca. April 2016        Submission of system description papers
* 11/12.08.2016        Presentation of systems and task results at WAC-X workshop (ACL 2016, Berlin)


## TASK FORCE:

### CMC data set:
* Michael Beißwenger (Technische Universität Dortmund)
* Kay-Michael Würzner (Berlin-Brandenburgische Akademie der Wissenschaften)

### Web corpora data set:
* Sabine Bartsch (Technische Universität Darmstadt)
* Stefan Evert (Universität Erlangen-Nürnberg)

## Contact
empirist@collocations.de
