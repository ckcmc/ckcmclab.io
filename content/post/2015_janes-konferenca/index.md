---
# Documentation: https://wowchemy.com/docs/managing-content/

title: "Corpus-linguistic and NLP aspects of Internet-based Slovene"
subtitle: "Related Conference Announcement"
authors:
- Michael Beißwenger
profile: false
tags: []
categories:
- conference
date: 2015-12-05T14:21:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  preview_only: true
  caption: ""
  focal_point: ""

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []

links:
- name: Conference website
  url: https://nl.ijs.si/janes/dogodki/konferenca-2015/
---

The members of the [JANES project](https://nl.ijs.si/janes) have been
organizing a conference on corpus-linguistic and NLP aspects of CMC discourse
([Slovenščina na spletu in v novih
medijih](https://nl.ijs.si/janes/dogodki/konferenca-2015/), University of
Ljubljana, 25.–27. November 2015).
