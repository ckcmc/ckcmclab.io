---
title: "Annotation schemas for CMC and social media genres: resources from the TEI special interest group on CMC"
subtitle: "Update"
summary: ""
authors:
- Michael Beißwenger
profile: false
tags: []
categories:
- cmc-tei
date: 2015-12-05T14:20:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

links:
- name: Wiki pages of the special interest group
  url: http://wiki.tei-c.org/index.php/SIG:Computer-Mediated_Communication
- name: Website of the Text Encoding Initiative
  url: http://www.tei-c.org/

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
---

The special interest group "computer-mediated communication" of the Text
Encoding Initiative (TEI) provides schemas for the annotation of cmc and social
media genres which are conformant with the TEI standards for text encoding and
which have been tested in corpus projects on French and German CMC.

The schemas have been presented and discussed as part of the [2015 cmc-corpora
conference in Rennes, France]({{< relref "cmc-corpora2015" >}}).
