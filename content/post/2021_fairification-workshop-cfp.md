---
title: Data Management for FAIR CMC corpora
subtitle: "Workshop: Call for Participation"
summary: ""
authors:
- Egon W. Stemle
profile: false
tags: []
categories:
- ckcmc
- call for participation
- workshop
date: 2021-10-02
featured: true
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: [cmc-corpora2021]
---

{{< cite page="/event/2021_fairification" view="2" >}}
