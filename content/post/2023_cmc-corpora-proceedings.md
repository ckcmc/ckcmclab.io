---
# Documentation: https://wowchemy.com/docs/managing-content/

title: "Proceedings of the 10th International Conference on CMC and Social Media Corpora for the Humanities 2023 (CMC-2023)"
subtitle: "New Publication"
authors:
- Egon W. Stemle
profile: false
tags: []
categories:
- publication
date: 2023-09-07
featured: false
draft: false
section_pager: true

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  preview_only: true
  caption: ""
  focal_point: ""

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: [cmc-corpora2023]

---

{{< cite page="/publication/cmc-corpora-2023" view="2" >}}
