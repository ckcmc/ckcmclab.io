---
title: "11th Conference on Computer-Mediated Communication (CMC) and Social Media Corpora"
subtitle: "Conference Announcement"
authors:
- Alexander König
profile: false
categories:
- conference
date: 2023-12-18
draft: false
featured: false
image:
  filename: featured
  focal_point: Smart
  preview_only: false
projects: [cmc-corpora2024]
---
Save the date! CMC2024 will be taking place in beautiful Nice, France, from 5-6 September, 2024!
The call for papers deadline will be 15 April, 2024. Call coming out early next year. Check out the website https://cmc-corpora-nice.sciencesconf.org
