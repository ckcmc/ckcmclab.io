---
# Documentation: https://wowchemy.com/docs/managing-content/

title: "CMC Corpora 2016 in Ljubljana, Slovenia"
subtitle: "Call for Papers"
authors:
- Michael Beißwenger
profile: false
tags: []
categories:
- call for papers
date: 2016-01-30T10:00:00
featured: false
draft: false
section_pager: true

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  preview_only: true
  caption: ""
  focal_point: ""

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: [cmc-corpora2016]

# {{< cite page="/project/cmc-corpora2016" view="2" >}}
---

We proudly present [the CfP for the 2016 issue of the cmc-corpora
conference](https://nl.ijs.si/janes/cmc-corpora2016/cfp/): 4th Conference on CMC
and Social Media Corpora for the Humanities, 27-28 September 2016, Ljubljana,
Slovenia.
