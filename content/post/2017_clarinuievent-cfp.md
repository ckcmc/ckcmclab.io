---
title: "How to use TEI for the annotation of CMC and social media resources: a practical introduction"
subtitle: "Workshop: Call for Participation"
summary: ""
authors:
- Egon W. Stemle
profile: false
tags: []
categories:
- call for participation
- workshop
date: 2017-07-26
featured: true
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: [cmc-corpora2017]
---

The goal of the event is to give a practical introduction into the annotation of language data from genres of computer-mediated communication (CMC) and social media using the formats of the Text Encoding Initiative (TEI). In an introductory section participants will learn about the general architecture of TEI encoding schemas and about rules for the creation of so-called customizations which allow for extending the use of TEI with textual genres and in domains which are not yet covered by the current version of the TEI guidelines. Examples for TEI customizations are the representation schemas for CMC/social media genres developed in the TEI special interest group “computer-mediated communication”.

<https://cmc-corpora2017.eurac.edu/uievent/>
