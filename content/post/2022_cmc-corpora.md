---
title: 9th Conference on Computer-Mediated Communication (CMC) and Social Media
  Corpora
subtitle: "Conference Announcement"
authors:
- Steven Coats
profile: false
categories:
- conference
date: 2022-03-07T10:25:12.400Z
draft: false
featured: false
image:
  filename: featured
  focal_point: Smart
  preview_only: false
projects: [cmc-corpora2022]
links:
- name: Conference website
  url: https://www.usc.es/en/congresos/cmc2022
---
The 9th Conference on Computer-Mediated Communication (CMC) and Social Media Corpora will be held in Santiago de Compostela, Spain, from Wednesday 28 to Thursday 29 September 2022. It will be hosted by the Faculty of Philology of the University of Santiago de Compostela. In the event that the pandemic situation makes travel difficult or impossible, the conference will take place in a hybrid or remote format.

‘CMC-corpora 2022’ is the 9th edition of an annual conference series dedicated to the development, analysis, and processing of corpora of computer-mediated communication and social media for research in the humanities. The conference brings together language-centered research on CMC and social media in linguistics, communication sciences, media studies, and social sciences with research questions from the fields of corpus and computational linguistics, language and text technology, and machine learning. 

<https://www.usc.es/en/congresos/cmc2022>
