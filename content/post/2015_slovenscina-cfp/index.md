---
# Documentation: https://wowchemy.com/docs/managing-content/

title: "Special issue of the open-access journal Slovenščina 2.0: Resources, tools and methods for analysing CMC"
subtitle: "Call for Papers"
authors:
- Michael Beißwenger
profile: false
tags: []
categories:
- call for papers
date: 2015-12-05T14:22:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  preview_only: true
  caption: ""
  focal_point: ""

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []

links:
- name: Publisher website
  url: http://www.trojina.org/slovenscina2.0/en/oddaja-prispevkov/
---

Call for papers for a special issue of the open-access journal Slovenščina 2.0:
"Resources, tools and methods for analysing computer-mediated communication"

You are kindly invited to submit your paper for a special issue of the
open-access journal Slovenščina 2.0 on resources, tools and methods for
analysing computer-mediated communication that will be published in August
2016. We welcome papers reporting on novel and completed research as well as
survey papers, position papers, review papers and project reports.

## The topics include but are not limited to:
* construction and distribution of CMC corpora
* tools and resources for processing of CMC
* corpus analyses of CMC
* comparisons of CMC with standard and/or spoken discourse
* sociolinguistic studies of CMC
* code-switching in CMC
* neologism and semantic shift detection in CMC
* offensive language in CMC

We welcome manuscripts written in English and Slovene. Please follow the
instructions for manuscript submission:
http://www.trojina.org/slovenscina2.0/en/oddaja-prispevkov/

## Important dates:
* 31. 03. 2016 – Submission of manuscripts
* 31. 05. 2016 – Notification of acceptance/rejection
* 31. 06. 2016 – Submission of final versions
* 15. 07. 2016 – Formatting of the special issue
* 30. 07. 2016 – Submission of proofs
* 15. 08. 2016 – Publication of the special issue

## Editors-in-chief:
* Nataša Logar (UL FDV)
* Polona Gantar (UL FF)

## Guest editor:
* Darja Fišer (UL FF)

## Reviewers:
* Michael Beißwenger (TUD)
* Helena Dobrovoljc (ZRC SAZU and FH UNG)
* Vojko Gorjanc (UL FF)
* Axel Herold (BBAW)
* Simon Krek (IJS)
* Lothar Lemnitzer (BBAW)
* Harald Lüngen (IDS)
* Dunja Mladenić (IJS)
* Marko Stabej (UL FF)
* Egon W. Stemle (Eurac Research)
* Marko Robnik Šikonja (UL FRI)
* Darinka Verdonik (UM FERI)
* Ciara R. Wigham (ICAR)
