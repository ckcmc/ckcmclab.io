---
title: "EmpiriST2015: Shared Task on annotating German CMC"
subtitle: "Shared Task Announcement"
summary: ""
authors:
- Michael Beißwenger
profile: false
tags: []
categories:
- shared task
date: 2015-12-05T14:19:00
lastmod: 2015-12-18T09:59:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

links:
- name: Shared task website
  url: https://sites.google.com/site/empirist2015/

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
---

*EmpiriST2015* is a community shared task on tokenization and part-of-speech
tagging of German CMC and social media data.Its goal is to encourage the
developers of NLP applications to adapt their tools and resources for the
processing of written CMC discourse. The website of the shared task provides
data samples, an extended PoS tagset for CMC and detailed annotation guidelines
for tokenizing and PoS tagging German CMC data.
