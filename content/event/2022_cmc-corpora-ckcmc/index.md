---
title: "CKCMC: CLARIN K(nowledge)-Centre for CMC and Social Media Corpora"
subtitle: "Presentation"
summary: 'Presentation at the 9th Conference on Computer-Mediated Communication (CMC) and Social Media Corpora.'

# event:
# event_url:
location: Faculty of Philology, University of Santiago de Compostela, ES

# Talk start and end times.
#   End time can optionally be hidden by prefixing the line with `#`.
date: "2022-09-22T17:30:00Z"
date_end: "2022-09-22T18:00:00Z"
all_day: false

# Schedule page publish date (NOT talk date).
publishDate: "2022-09-28T00:00:00Z"

profile: false
authors:
- Egon W. Stemle
- Jennifer-Carmen Frey
- Alexander König
- Achille Falaise
- Tomaž Erjavec
- Harald Lüngen

tags: []

# Is this a featured talk? (true/false)
featured: false

image:
  caption: ''
  focal_point: Right


url_code: ""
url_pdf: ""
url_slides: ""
url_video: ""
links:
  - name: Paper
    url: '/publications/cmc-corpora-2022-ckcmc/cmc-corpora-2022-ckcmc.pdf'

# Markdown Slides (optional).
#   Associate this talk with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides = "example-slides"` references `content/slides/example-slides.md`.
#   Otherwise, set `slides = ""`.
slides:

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: [cmc-corpora2022]

categories:
- ckcmc
- presentation
---

## About

The CLARIN Knowledge Centre for Computer-Mediated Communication and Social
Media Corpora (CKCMC) offers expertise on language resources and technologies
for ComputerMediated Communication and Social Media. Its basic activities are
to A) Give researchers, students, and other interested parties information
about the available resources, technologies, and community activities, B)
Support interested parties in producing, modifying or publishing relevant
resources and technologies and C) Organise training activities. Additionally,
the CKCMC manages the cmc-corpora.org website and helps in curating the CLARIN
CMC Resource Family. The CKCMC can be reached via a Helpdesk (e-Mail). In this
talk, we first want to introduce the CKCMC to the CMC-community; secondly, we
would like to discuss with and get input from the CMC community on how to
better serve our community goals or what further goals to pursue.
