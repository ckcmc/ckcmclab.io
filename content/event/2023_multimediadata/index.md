---
title: "Workshop: Working with Multimedia Data"
subtitle:
summary: The workshop was held in conjunction with the CMC corpora 2023 conference.

# event: CMC-Corpora and CKCMC Workshop
# event_url: https://www.clarin.eu/event/2021/data-management-fair-cmc-corpora
location: Aula O102, Schloss, University of Mannheim (Germany)

# Talk start and end times.
#   End time can optionally be hidden by prefixing the line with `#`.
date: "2023-09-15T15:30:00Z"
date_end: "2023-09-15T17:00:00Z"
all_day: false

# Schedule page publish date (NOT talk date).
publishDate: "2023-08-21T00:00:00Z"

profile: false
authors:
- Thomas Schmidt

tags: []

# Is this a featured talk? (true/false)
featured: true

image:
  caption: 'Image credit: **Medientechnik Universität Mannheim**'
  focal_point: Right

url_code: ""
url_pdf: ""
url_slides: "CMC_Workshop_Thomas_Schmidt.pdf"
url_video: ""
#links:
#- name: Related conference
#  url: conferences/cmc-corpora2021

# Markdown Slides (optional).
#   Associate this talk with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides = "example-slides"` references `content/slides/example-slides.md`.
#   Otherwise, set `slides = ""`.
slides:

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: [cmc-corpora2023]

categories:
- ckcmc
- workshop
---

## About

The workshop gave a basic introduction to methodological and technological
aspects of working with multimedia data as part of CMC corpora. It gave an
overview of the most important tools for manual transcription of audio or
video, discussed the role of automatic methods (ASR, Automatic Speech
Recognition) and looked at standardisation in the TEI framework, including
integration with solutions for written CMC data. Researchers of all stages were
welcome, no special equipment were required to follow the workshop.


## Organizing Committee

* {{% mention "alexander könig" %}}
* {{% mention "egon w. stemle" %}}
