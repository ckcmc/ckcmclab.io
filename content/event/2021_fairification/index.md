---
title: "Workshop: Data Management for FAIR CMC corpora"
subtitle:
summary: The virtual workshop was held in conjunction with the CMC corpora 2021 conference.

# event:
# event_url:
location: CLARIN virtual Zoom meeting

# Talk start and end times.
#   End time can optionally be hidden by prefixing the line with `#`.
date: "2021-10-27T10:00:00Z"
date_end: "2021-10-27T16:00:00Z"
all_day: false

# Schedule page publish date (NOT talk date).
publishDate: "2021-09-28T00:00:00Z"

profile: false
authors:
- Jennifer-Carmen Frey
- Egon W. Stemle
- Alexander König
- Inge Slouwerhof
- Harald Lüngen
- Michael Beißwenger

tags: []

# Is this a featured talk? (true/false)
featured: false

image:
  caption: 'Image credit: [**Eurac Reserach**](https://www.eurac.edu/en/meeting-management)'
  focal_point: Right

url_code: ""
url_pdf: ""
url_slides: ""
url_video: ""
links:
- name: Recording
  url: https://www.youtube.com/playlist?list=PLlKmS5dTMgw0BgpZRPYtFy-t99c0j6iM3
- name: Slides
  url: https://www.clarin.eu/event/2021/workshop-data-management-fair-cmc-corpora#programme
- name: CLARIN-ERIC event page
  url: https://www.clarin.eu/event/2021/data-management-fair-cmc-corpora
- name: CLARIN-ERIC blog post
  url: https://www.clarin.eu/blog/recap-workshop-data-management-fair-cmc-corpora

# Markdown Slides (optional).
#   Associate this talk with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides = "example-slides"` references `content/slides/example-slides.md`.
#   Otherwise, set `slides = ""`.
slides:

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: [cmc-corpora2021]

categories:
- ckcmc
- workshop
---

## About

The workshop detailled important aspects of data management as it applies to
CMC corpora, for example, how data should be collected, which metadata to
record, which data formats are advisable, and which legal concerns need to be
considered when CMC corpora are supposed to be Findable, Accessible,
Interoperable and Reusable at the end of a project. The workshop was aimed at
early career researchers or researchers that were about to start a new project
in the field of CMC corpora.

The virtual workshop was organised with support from
[CLARIN-ERIC](https://www.clarin.eu/event/2021/data-management-fair-cmc-corpora).


## Organizing Committee

* {{% mention "jennifer-carmen frey" %}}
* {{% mention "alexander könig" %}}
* {{% mention "egon w. stemle" %}}
