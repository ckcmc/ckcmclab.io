---
widget: blank  # See https://sourcethemes.com/academic/docs/page-builder/
headless: true  # This file represents a page section.
active: true  # Activate this widget? true/false
weight: 5  # Order that this section will appear.

title: "Proceedings Series"
subtitle: "Conference on CMC and Social Media Corpora"

design:
  columns: '1'
---



*cmc-corpora* (https://cmc-corpora.org) is a series of yearly conferences
established in 2013 and dedicated to the collection, annotation, processing,
and analysis of corpora of computer-mediated communication (CMC) and social
media.

The conferences bring together language-centered research on CMC and social
media in linguistics, philologies, communication sciences, media, and social
sciences with research questions from the fields of corpus and computational
linguistics, language technology, text technology, and machine learning. The
conferences feature research in which computational methods and tools are used
for language-centered empirical analysis of CMC and social media phenomena as
well as research on building, processing, annotating, representing, and
exploiting CMC and social media corpora, including their integration in digital
research infrastructures.

## Publication

Papers accepted by the Scientific Committee and presented during the conference
are included in the volume of conference proceedings published by members of
the steering committee and the organizing committee of the respective
conference.

The papers must report original, previously unpublished findings in the field.
Papers are reviewed by at least two members of the international Scientific
Committee, must pass their judgement, and need to incorporate the feed-back
from this process. The single paper length may vary with the topical focus of
the conference, but it is usually in the range of 3-8 pages. Please, refer to
the specific conference for more details.

