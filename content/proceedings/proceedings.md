---
# An instance of the Blank widget.
# Documentation: https://sourcethemes.com/academic/docs/page-builder/
widget: pages

# Activate this widget? true/false
active: true

# This file represents a page section.
headless: true

# Order that this section appears on the page.
weight: 10

title: Proceedings
subtitle: Conference Proceedings


content:
  # Page type to display. E.g. post, event, or publication.
  page_type: publication
  # Choose how much pages you would like to display (0 = all pages)
  count: 0
  # Choose how many pages you would like to offset by
  offset: 0
  # Page order. Descending (desc) or ascending (asc) date.
  order: desc
  # Optionally filter posts by a taxonomy term.
  filters:
    tag: ''
    category: 'cmc-corpora conference proceeding'
    publication_type: '9'
    exclude_featured: false
    exclude_past: false
    exclude_future: false

design:
  columns: "1"
  # background:
  #   # image: headers/bubbles-wide.jpg
  #   image_darken: 0.6
  #   image_parallax: true
  #   image_position: center
  #   image_size: cover
  #   # text_color_light: true
  # Toggle between the various page layout types.
  #   1 = List
  #   2 = Compact
  #   3 = Card
  #   4 = Citation (publication only)  
  view: 4
---
